use std::io::{self, BufRead};
use std::collections::HashMap;

fn attack(memo: &mut HashMap<i64, i64>, h: i64) -> i64 {
  if h == 1 {
    1
  } else if memo.contains_key(&h) {
    memo[&h]
  } else {
    let x = h / 2;
    1 + 2 * attack(memo, x)
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let h: i64 = line.parse().unwrap();
  let mut memo = HashMap::new();
  let count = attack(&mut memo, h);
  println!("{}", count);
}