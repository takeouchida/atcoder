use std::io::{self, BufRead};
use std::cmp::Ordering;

pub fn lower_bound<T: Ord>(vec: &[T], val: T) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

#[derive(PartialEq, Eq, Debug, Copy, Clone, Default, Hash)]
struct Z(i32, i32);

impl PartialOrd for Z {
    #[inline]
    fn partial_cmp(&self, other: &Z) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }

    #[inline]
    fn lt(&self, other: &Self) -> bool {
        self.0 < other.0
    }
    #[inline]
    fn le(&self, other: &Self) -> bool {
        self.0 <= other.0
    }
    #[inline]
    fn gt(&self, other: &Self) -> bool {
        self.0 > other.0
    }
    #[inline]
    fn ge(&self, other: &Self) -> bool {
        self.0 >= other.0
    }
}

impl Ord for Z {
    #[inline]
    fn cmp(&self, other: &Z) -> Ordering {
        self.0.cmp(&other.0)
    }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i32> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let h = vs[0];
  let n = vs[1];
  let mut mgs = Vec::new();
  for _ in 0..n {
    let line = lines.next().unwrap().unwrap();
    let xs: Vec<i32> = line.split(' ').map(|x| x.parse().unwrap()).collect();
    mgs.push((xs[0], xs[1]));
  }
  let mut mgs2: Vec<Z> = mgs.iter().map(|(x, y)| Z(*x, *y)).collect();
  mgs2.sort();
  let mut min = mgs2[(n-1) as usize].1;
  for i in (0..(n as usize - 1)).rev() {
    if mgs2[i].1 < min {
      min = mgs2[i].1;
    } else {
      mgs2[i].1 = min;
    }
  }
  let ans = (0..(n as usize)).map(|i| {
    let d1 = h / mgs[i].0;
    let m = h % mgs[i].0;
    let j = lower_bound(&mgs2, Z(m, 0));
    let d2 = if m == 0 {
      0
    } else {
      mgs2[j].1
    };
    let total = d1 * mgs[i].1 + d2;
    total
  }).min().unwrap();
  println!("{}", ans);
}