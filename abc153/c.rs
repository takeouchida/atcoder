use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let k = vs[1];
  let line = lines.next().unwrap().unwrap();
  let mut hs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  hs.sort();
  let ans = if n <= k {
    0
  } else {
    hs.iter().take((n - k) as usize).sum::<i64>()
  };
  println!("{}", ans);
}