use std::io::{self, BufRead};

fn gcd(x: usize, y: usize) -> usize {
  let (mut a, mut b) = if x < y {
    (y, x)
  } else {
    (x, y)
  };
  while b != 0 {
    let r = a % b;
    a = b;
    b = r;
  }
  a
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, k, s) = (xs[0], xs[1], xs[2]);
  let mut t = 2;
  while gcd(s, t) != 1 {
    t += 1;
  }
  for i in 0..k {
    if i > 0 {
      print!(" ");
    }
    print!("{}", s);
  }
  for i in 0..(n-k) {
    if k > 0 || i > 0 {
      print!(" ");
    }
    print!("{}", t);
  }
  println!();
}