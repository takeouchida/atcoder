use std::io::{self, BufRead};
use std::collections::HashSet;

fn create_tree(tree: &mut Vec<HashSet<usize>>, edges: &Vec<HashSet<usize>>, r: usize) {
  let children: HashSet<usize> = edges[r].iter().filter(|e| tree[**e].is_empty()).map(|e| *e).collect();
  tree[r] = children.clone();
  for c in children {
    create_tree(tree, edges, c);
  }
}

fn count_color(counts: &mut Vec<usize>, passed: &mut Vec<bool>, colors: &mut Vec<usize>, tree: &Vec<HashSet<usize>>, cs: &Vec<usize>, r: usize, l: usize) {
  let c = cs[l];
  if passed[c] {
    for i in 0..colors.len() {
      if r <= l {
        counts[colors[i]] += 1;
      }
    }
    for child in &tree[l] {
      count_color(counts, passed, colors, tree, cs, r, *child);
    }
  } else {
    passed[c] = true;
    colors.push(c);
    for i in 0..colors.len() {
      if r <= l {
        counts[colors[i]] += 1;
      }
    }
    for child in &tree[l] {
      count_color(counts, passed, colors, tree, cs, r, *child);
    }
    colors.pop();
    passed[c] = false;
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: usize = line.parse().unwrap();
  let line = lines.next().unwrap().unwrap();
  let cs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).map(|c: usize| c - 1).collect();
  let mut es: Vec<HashSet<usize>> = vec![HashSet::new(); n];
  for _ in 0..(n-1) {
    let line = lines.next().unwrap().unwrap();
    let xs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
    let x = xs[0] - 1;
    let y = xs[1] - 1;
    es[x].insert(y);
    es[y].insert(x);
  }

  let mut counts: Vec<usize> = vec![0; n];
  for r in 0..n {
    let mut tree: Vec<HashSet<usize>> = vec![HashSet::new(); n];
    create_tree(&mut tree, &es, r);
    let mut passed: Vec<bool> = vec![false; n];
    let mut colors: Vec<usize> = Vec::new();
    count_color(&mut counts, &mut passed, &mut colors, &tree, &cs, r, r);
  }

  for c in counts {
    println!("{}", c);
  }
}