use std::io::{self, BufRead};

const M: i64 = 1000000007;

fn sum_range(a: i64, b: i64) -> i64 {
  (a + b) * (b - a + 1) / 2
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let k = vs[1];
  let mut total = 0;
  for i in k..=(n+1) {
    let min = sum_range(0, i - 1);
    let max = sum_range(n + 1 - i, n);
    total = (total + max - min + 1) % M;
  }
  println!("{}", total);
}