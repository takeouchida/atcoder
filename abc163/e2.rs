use std::io::{self, BufRead};
use std::cmp::max;

fn abs(x: usize, y: usize) -> usize {
  if x < y { y - x } else { x - y }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: usize = line.parse().unwrap();
  let line = lines.next().unwrap().unwrap();
  let mut xs: Vec<(usize, usize)> = line.split(' ').map(|x| x.parse().unwrap()).enumerate().collect();
  xs.sort_by(|x, y| y.1.cmp(&x.1));
  let mut memo: Vec<Vec<usize>> = vec![vec![0; n+1]; n+1];
  for k in 1..=n {
    let (p, a) = xs[k-1];
    for i in 0..=k {
      let j = k - i;
      if i == 0 {
        memo[i][j] = memo[i][j-1] + a * abs(p, n-j);
      } else if j == 0 {
        memo[i][j] = memo[i-1][j] + a * abs(p, i-1);
      } else {
        memo[i][j] = max(memo[i][j-1] + a * abs(p, n-j), memo[i-1][j] + a * abs(p, i-1));
      }
    }
  }
  let ans = (0..=n).map(|k| memo[k][n-k]).max().unwrap();
  println!("{}", ans);
}