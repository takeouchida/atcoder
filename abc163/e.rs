use std::io::{self, BufRead};

fn abs_sub(x: usize, y: usize) -> usize {
  if x < y { y - x } else { x - y }
}

fn process(xs: &Vec<usize>, moved: &Vec<bool>, mut lo: usize, mut hi: usize) -> usize {
  let mut result = 0;
  let mut moved = moved.clone();
  while lo < hi {
    let mut ys: Vec<_> = (0..moved.len()).filter(|y| !moved[*y]).map(|y| {
      let hi_val = abs_sub(hi - 1, y) * xs[y];
      let lo_val = abs_sub(lo, y) * xs[y];
      if lo_val < hi_val {
        (hi_val, y, true)
      } else {
        (lo_val, y, false)
      }
    }).collect();
    ys.sort_by(|t1, t2| t2.0.cmp(&t1.0));
    let (max_val, y, is_hi) = ys[0];
    let max_tups: Vec<_> = ys.iter().take_while(|t| t.0 == max_val).collect();
    if max_tups.len() == 1 {
      moved[y] = true;
      result += max_val;
      if is_hi {
        hi -= 1;
      } else {
        lo += 1;
      }
    } else {
      let rest_result = max_tups.iter().map(|(_, y, is_hi)| {
        let mut moved = moved.clone();
        moved[*y] = true;
        if *is_hi {
          process(&xs, &moved, lo, hi - 1)
        } else {
          process(&xs, &moved, lo + 1, hi)
        }
      }).max().unwrap();
      return result + max_val + rest_result;
    }
  }
  result
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let _ = lines.next();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let moved = vec![false; xs.len()];
  let ans = process(&xs, &moved, 0, xs.len());
  println!("{}", ans);
}