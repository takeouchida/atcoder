use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: usize = line.parse().unwrap();
  let line = lines.next().unwrap().unwrap();
  let xs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).map(|x: usize| x - 1).collect();
  let mut men = vec![0; n];
  for x in xs {
    men[x] += 1;
  }
  for m in men {
    println!("{}", m);
  }
}