use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn _dfs(tree: &Vec<BTreeSet<usize>>, visited: &mut Vec<bool>, parents: &mut Vec<usize>, node: usize) -> ((usize, usize), (usize, usize, usize, usize)) {
  visited[node] = true;
  let count = tree[node].iter().filter(|c| !visited[**c]).count();
  if count == 0 {
    return ((0, node), (0, node, node, node));
  }
  let mut results = Vec::new();
  for &child in &tree[node] {
    if visited[child] {
      continue;
    }
    parents[child] = node;
    if count == 1 {
      let ((n1, c1), (n2, c2, c3, c4)) = _dfs(tree, visited, parents, child);
      return ((1 + n1, c1), (n2, c2, c3, c4));
    }
    results.push(_dfs(tree, visited, parents, child));
  }
  results.sort_by(|x, y| (y.0).0.cmp(&(x.0).0));
  let ((n11, c11), (n2, c2, c3, c4)) = results[0];
  let ((n12, c12), _) = results[1];
  if n2 < 2 + n11 + n12 {
    ((1 + n11, c11), (2 + n11 + n12, node, c11, c12))
  } else {
    ((1 + n11, c11), (n2, c2, c3, c4))
  }
}

fn dfs(tree: &Vec<BTreeSet<usize>>, visited: &mut Vec<bool>, parents: &mut Vec<usize>, root: usize) -> (usize, usize, usize, usize) {
  let ((n1, c1), (n2, c2, c3, c4)) = _dfs(tree, visited, parents, root);
  if n1 < n2 {
    (n2, c2, c3, c4)
  } else {
    (n1, root, c1, root)
  }
}

fn find_path(parents: &Vec<usize>, root: usize, leaf1: usize, leaf2: usize) -> Vec<usize> {
  let mut path = Vec::new();
  let mut p = leaf1;
  while p != root {
    path.push(p);
    p = parents[p];
  }
  path.push(p);
  if leaf2 != root {
    let mut path2 = Vec::new();
    let mut p = leaf2;
    while p != root {
      path2.push(p);
      p = parents[p];
    }
    for p in path2.iter().rev() {
      path.push(*p);
    }
  }
  path
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut tree: Vec<BTreeSet<usize>> = vec![BTreeSet::new(); n];
  for _ in 0..(n-1) {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (u, v) = (xs[0] - 1, xs[1] - 1);
    tree[u].insert(v);
    tree[v].insert(u);
  }
  let mut visited = vec![false; n];
  let mut parents = vec![0; n];
  let (_, root, leaf1, leaf2) = dfs(&tree, &mut visited, &mut parents, 0);
  let path = find_path(&parents, root, leaf1, leaf2);
  let mut answer: Vec<usize> = vec![1];
  for i in 1..(path.len()-1) {
    let valid = tree[path[i]].iter().all(|k| *k == path[i-1] || *k == path[i+1] || tree[*k].len() == 1);
    if !valid {
      println!("-1");
      return;
    }
    let base = answer.len() + 1;
    let count = tree[path[i]].len() - 2;
    for i in 0..count {
      answer.push(base + i + 1);
    }
    answer.push(base);
  }
  let z = answer.len() + 1;
  answer.push(z);
  print!("{}", answer[0]);
  for i in 1..answer.len() {
    print!(" {}", answer[i]);
  }
  println!();
}