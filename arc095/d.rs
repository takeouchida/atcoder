use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let _ = lines.next();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let n = xs.iter().max().unwrap();
  let k = xs.iter().max_by(|x, y| min(**x, n - *x).cmp(&min(**y, n - *y))).unwrap();
  println!("{} {}", n, k);
}