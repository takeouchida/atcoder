use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut ys = xs.clone();
  ys.sort();
  let mid = ys.len() / 2;
  if ys[mid] == ys[mid-1] {
    for _ in 0..n {
      println!("{}", ys[mid]);
    }
  } else {
    for x in xs {
      if x < ys[mid] {
        println!("{}", ys[mid]);
      } else {
        println!("{}", ys[mid-1]);
      }
    }
  }
}