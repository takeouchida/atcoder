use std::io::{self, BufRead};
use std::collections::BTreeMap;

fn check_stats(stats: &Vec<BTreeMap<char, usize>>, h: usize) -> bool {
  let mut stats_checked = vec![false; h];
  for i in 0..h {
    if stats_checked[i] {
      continue;
    }
    for j in (i+1)..h {
      if stats[i] == stats[j] {
        stats_checked[i] = true;
        stats_checked[j] = true;
        break;
      }
    }
  }
  if h % 2 == 0 {
    stats_checked.into_iter().all(|x| x)
  } else {
    stats_checked.into_iter().filter(|x| *x).count() == h - 1
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (h, w) = (xs[0], xs[1]);
  let mut css: Vec<Vec<char>> = Vec::new();
  for _ in 0..h {
    css.push(lines.next().unwrap().unwrap().chars().collect());
  }
  let mut rows: Vec<BTreeMap<char, usize>> = Vec::new();
  for i in 0..h {
    let mut row: BTreeMap<char, usize> = BTreeMap::new();
    for j in 0..w {
      let v = 1 + row.get(&css[i][j]).unwrap_or(&0);
      row.insert(css[i][j], v);
    }
    rows.push(row);
  }
  if !check_stats(&rows, h) {
    println!("NO");
    return;
  }
  let mut cols: Vec<BTreeMap<char, usize>> = Vec::new();
  for j in 0..w {
    let mut col: BTreeMap<char, usize> = BTreeMap::new();
    for i in 0..h {
      let v = 1 + col.get(&css[i][j]).unwrap_or(&0);
      col.insert(css[i][j], v);
    }
    cols.push(col);
  }
  println!("{}", if check_stats(&cols, w) { "YES" } else { "NO" });
}