use std::io::{self, BufRead};

fn dfs(es: &Vec<Vec<usize>>, ps: &Vec<usize>, cs: &mut Vec<usize>, n: usize, c: usize) {
  cs[n] = c;
  for &p in &es[n] {
    dfs(es, ps, cs, p, c + ps[p]);
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, q) = (xs[0], xs[1]);
  let mut es: Vec<Vec<usize>> = vec![vec![]; n];
  for _ in 1..n {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    es[xs[0]-1].push(xs[1]-1);
  }
  let mut ps: Vec<usize> = vec![0; n];
  for _ in 0..q {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    ps[xs[0]-1] += xs[1];
  }
  let mut cs: Vec<usize> = vec![0; n];
  dfs(&es, &ps, &mut cs, 0, ps[0]);
  for &c in &cs {
    print!("{} ", c);
  }
  println!();
}