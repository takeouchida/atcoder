use std::io::{self, BufRead};

pub fn lower_bound<T: Ord>(vec: &[T], val: T) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

fn to_index(c: char) -> usize {
  c as usize - 'a' as usize
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let s: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
  let t: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
  let mut cs: Vec<Vec<usize>> = vec![vec![]; 26];
  for i in 0..s.len() {
    let j = to_index(s[i]);
    cs[j].push(i);
  }
  let mut i: usize = 0;
  let mut round: usize = 0;
  for &c in &t {
    let j = to_index(c);
    if cs[j].len() == 0 {
      println!("-1");
      return;
    }
    let mut k = lower_bound(&cs[j], i);
    if k == cs[j].len() {
      k = 0;
      round += 1;
    }
    i = cs[j][k] + 1;
  }
  println!("{}", round * s.len() + i);
}