use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut vs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  vs.sort();
  let mut ans: f64 = vs[0] as f64;
  for i in 1..n {
    ans = (ans + vs[i] as f64) / 2.0;
  }
  println!("{}", ans);
}