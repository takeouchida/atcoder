use std::io::{self, BufRead};
use std::collections::HashSet;

const M: i64 = 1000000007;

pub fn find_prime_factors(n: i64) -> HashSet<i64> {
  let mut res = HashSet::new();
  let mut i = 2;
  let mut m = n;
  while i * i <= m {
    while m % i == 0 {
      let val = 1 + res.get(&i).unwrap_or(&0);
      res.insert(i);
      m /= i;
    }
    i += 1
  }
  if m != 1 {
    let val = 1 + res.get(&m).unwrap_or(&0);
    res.insert(m);
  }
  res
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let k = vs[1];

  for i in 1..=k {
    for j in 1..=k {
      let mut fi = find_prime_factors(i);
      let fj = find_prime_factors(j);
      for f in fj {
        fi.insert(f);
      }
      println!("{} {} {:?}", );
    }
  }
}