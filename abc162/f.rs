use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: usize = line.parse().unwrap();
  let line = lines.next().unwrap().unwrap();
  let xs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let mut memo: Vec<i64> = vec![0; n];
  let mut leftmost: i64 = xs[0];
  for i in 1..n {
    if i == 1 {
      memo[i] = max(xs[0], xs[1]);
    } else if i % 2 == 0 {
      leftmost += xs[i];
      memo[i] = max(memo[i-2] + xs[i], memo[i-1]);
    } else {
      memo[i] = max(leftmost, memo[i-2] + xs[i]);
    }
  }
  println!("{}", memo[n-1]);
}