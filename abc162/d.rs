use std::io::{self, BufRead};

fn is_valid(i: usize, j: usize, k: usize, a: char, b: char, c: char) -> bool {
  (j - i != k - j) &&
  ((a == 'R' && (b == 'G' && c == 'B' || b == 'B' && c == 'G')) ||
  (a == 'G' && (b == 'B' && c == 'R' || b == 'R' && c == 'B')) ||
  (a == 'B' && (b == 'R' && c == 'G' || b == 'G' && c == 'R')))
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let s: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
  let mut count: usize = 0;
  for i in 0..(n-2) {
    for j in (i+1)..(n-1) {
      for k in (j+1)..n {
        if is_valid(i, j, k, s[i], s[j], s[k]) {
          count += 1;
        }
      }
    }
  }
  println!("{}", count);
}