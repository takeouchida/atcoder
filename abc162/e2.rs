use std::io::{self, BufRead};

const M: i64 = 1000000007;

fn pow(p: i64, n: i64) -> i64 {
  let mut q = n;
  let mut m = p;
  let mut ans = 1;
  while q > 0 {
    if q % 2 == 1 {
      ans = (ans * m) % M;
    }
    q /= 2;
    m = (m * m) % M;
  }
  ans
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let k = vs[1];

  let mut counts: Vec<i64> = vec![0; k];
  for i in 0..k {
    let x = k - i;
    let mut total = pow((k / x) as i64, n as i64);
    let mut xx = 2 * x;
    while xx <= k {
      total = (total - counts[xx as usize - 1]) % M;
      xx += x;
    }
    counts[x as usize - 1] = total
  }

  let ans = counts.iter().enumerate().map(|(i, c)| *c * (i as i64 + 1) % M).fold(0, |acc, x| (acc + x) % M);
  println!("{}", ans);
}