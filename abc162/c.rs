use std::io::{self, BufRead};

fn gcd(x: usize, y: usize) -> usize {
  let (mut a, mut b) = if x < y {
    (y, x)
  } else {
    (x, y)
  };
  while b != 0 {
    let r = a % b;
    a = b;
    b = r;
  }
  a
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: usize = line.parse().unwrap();

  let mut memo = vec![vec![0; n]; n];
  for i in 0..n {
    for j in 0..n {
      memo[i][j] = gcd(i+1, j+1);
    }
  }
  let mut total: usize = 0;
  for i in 0..n {
    for j in 0..n {
      for k in 0..n {
        total += memo[memo[i][j]-1][k];
      }
    }
  }
  println!("{}", total);
}