use std::io::{self, BufRead};

fn last_char(a: char, b: char) -> Option<(usize, char)> {
  if a == 'R' && b == 'G' || a == 'G' && b == 'R' {
    Some((2, 'B'))
  } else if a == 'G' && b == 'B' || a == 'B' && b == 'G' {
    Some((0, 'R'))
  } else if a == 'B' && b == 'R' || a == 'R' && b == 'B' {
    Some((1, 'G'))
  } else {
    None
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let s: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
  let mut memo = vec![vec![0; 3]; n];
  if n < 3 {
    println!("0");
    return;
  }

  memo[n-1][0] = if s[n-1] == 'R' { 1 } else { 0 };
  memo[n-1][1] = if s[n-1] == 'G' { 1 } else { 0 };
  memo[n-1][2] = if s[n-1] == 'B' { 1 } else { 0 };

  for i in 1..n {
    memo[n-i-1][0] = memo[n-i][0] + (if s[n-i-1] == 'R' { 1 } else { 0 });
    memo[n-i-1][1] = memo[n-i][1] + (if s[n-i-1] == 'G' { 1 } else { 0 });
    memo[n-i-1][2] = memo[n-i][2] + (if s[n-i-1] == 'B' { 1 } else { 0 });
  }

  let mut count: usize = 0;
  for i in 0..(n-2) {
    for j in (i+1)..(n-1) {
      if let Some((l, c)) = last_char(s[i], s[j]) {
        let k = j * 2 - i;
        count += memo[j+1][l] - (if k < n && s[k] == c { 1 } else { 0 });
      }
    }
  }
  println!("{}", count);
}