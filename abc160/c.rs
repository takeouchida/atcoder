use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let ps: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let k = xs[0];
  let n = xs[1];

  let mut max_dist = ps[0] + k - ps[n as usize - 1];
  for i in 1..n {
    let dist = ps[i as usize] - ps[i as usize - 1];
    if max_dist < dist {
      max_dist = dist;
    }
  }
  println!("{}", k - max_dist);
}