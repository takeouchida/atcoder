use std::io::{self, BufRead};
use std::cmp::min;

fn dist(s: i32, t: i32, x: i32, y: i32) -> i32 {
  min(t - s, (x - s).abs() + 1 + (t - y).abs())
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let n = xs[0];
  let x = xs[1] - 1;
  let y = xs[2] - 1;

  let mut counts = vec![0; n as usize - 1];
  for i in 0..n {
    for j in (i+1)..n {
      let d = dist(i, j, x, y);
      counts[d as usize - 1] += 1;
    }
  }

  for i in 0..(n-1) {
    println!("{}", counts[i as usize]);
  }
}