use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let s: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
  let ans = if s[2] == s[3] && s[4] == s[5] { "Yes" } else { "No" };
  println!("{}", ans);
}