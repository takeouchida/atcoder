use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let mut x: i32 = lines.next().unwrap().unwrap().parse().unwrap();
  let q1 = x / 500;
  x %= 500;
  let q2 = x / 5;
  let h = q1 * 1000 + q2 * 5;
  println!("{}", h);
}