use std::io::{self, BufRead};
use std::collections::HashSet;
use std::mem::swap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let n = xs[0];
  let x = xs[1] - 1;
  let y = xs[2] - 1;
  let mut min_dists = vec![vec![-1; n as usize]; n as usize];

  for beg in 0..n {
    let mut vacant: HashSet<i32> = (0..n).collect();
    vacant.remove(&beg);
    let mut ps = vec![beg; 1];
    let mut dist = 0;
    while !vacant.is_empty() {
      let mut qs = Vec::new();
      for p in &ps {
        if *p == x && vacant.contains(&y) {
          vacant.remove(&y);
          qs.push(y);
          if min_dists[beg as usize][y as usize] == -1 || dist < min_dists[beg as usize][y as usize] {
            min_dists[beg as usize][y as usize] = dist;
          }
        }
        let p1 = *p + 1;
        if p1 < n && vacant.contains(&p1) {
          vacant.remove(&p1);
          qs.push(p1);
          if min_dists[beg as usize][p1 as usize] == -1 || dist < min_dists[beg as usize][p1 as usize] {
            min_dists[beg as usize][p1 as usize] = dist;
          }
        }
        let p2 = *p - 1;
        if p2 >= 0 && vacant.contains(&p2) {
          vacant.remove(&p2);
          qs.push(p2);
          if min_dists[beg as usize][p2 as usize] == -1 || dist < min_dists[beg as usize][p2 as usize] {
            min_dists[beg as usize][p2 as usize] = dist;
          }
        }
      }
      swap(&mut ps, &mut qs);
      dist += 1;
    }
  }

  for k in 0..(n-1) {
    let mut count = 0;
    for i in 0..n {
      for j in (i+1)..n {
        if min_dists[i as usize][j as usize] == k {
          count += 1;
        }
      }
    }
    println!("{}", count);
  }
}