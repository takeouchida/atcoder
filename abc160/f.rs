use std::io::{self, BufRead};
use std::collections::{HashSet, HashMap};
use std::ops::{Mul, Div, Rem};
use std::cmp::Ordering;

fn find_prime_factors(n: i64) -> HashMap<i64, i64> {
  let mut res = HashMap::new();
  let mut i = 2;
  let mut m = n;
  while i * i <= m {
    while m % i == 0 {
      let val = 1 + res.get(&i).unwrap_or(&0);
      res.insert(i, val);
      m /= i;
    }
    i += 1
  }
  if m != 1 {
    let val = 1 + res.get(&m).unwrap_or(&0);
    res.insert(m, val);
  }
  res
}

struct Decom {
  fs: HashMap<i64, i64>
}

impl Decom {
  pub fn new(n: i64) -> Self {
    Decom { fs: find_prime_factors(n) }
  }

//   pub fn to_i64(&self) -> i64 {
//     let mut result = 1;
//     for (k, v) in self.fs.iter() {
//       for _ in 0..*v {
//         result *= k;
//       }
//     }
//     result
//   }
}

impl Mul for Decom {
  type Output = Self;

  fn mul(self, rhs: Self) -> Self {
    let mut result = self.fs.clone();
    for (k, v) in rhs.fs.iter() {
      match self.fs.get(k) {
        None    => result.insert(*k, *v),
        Some(n) => result.insert(*k, n + *v),
      };
    }
    Decom { fs: result }
  }
}

impl Div for Decom {
  type Output = Option<Self>;

  fn div(self, rhs: Self) -> Option<Self> {
    let mut result = self.fs.clone();
    for (k, v) in rhs.fs.iter() {
      match self.fs.get(k) {
        None => return None,
        Some(n) => match n.cmp(v) {
          Ordering::Less => { return None; },
          Ordering::Equal => { result.remove(k); },
          Ordering::Greater => { result.insert(*k, *n - v); },
        },
      }
    }
    Some(Decom{ fs: result })
  }
}

impl Rem<i64> for Decom {
  type Output = i64;

  fn rem(self, rhs: i64) -> i64 {
    let mut result = 1;
    for (k, v) in self.fs.iter() {
      for _ in 0..*v {
        result = (result * *k) % rhs
      }
    }
    result
  }
}

fn fac(n: i64) -> Decom {
  let mut result = Decom::new(1);
  for i in 2..(n+1) {
    result = result * Decom::new(i);
  }
  result
}

fn dfs(tree: &Vec<Vec<i64>>, visited: &mut HashSet<i64>, n: i64) -> (i64, Decom) {
  visited.insert(n);
  let mut cs: Vec<i64> = Vec::new();
  for i in &tree[n as usize] {
    if !visited.contains(i) {
      cs.push(*i);
    }
  }
  if cs.len() == 1 {
    let (count, combo) = dfs(tree, visited, cs[0]);
    visited.remove(&n);
    return (count + 1, combo)
  }
  let mut counts: Vec<(i64, Decom)> = Vec::new();
  for i in cs {
    counts.push(dfs(tree, visited, i));
  }
  let total: i64 = counts.iter().map(|x| x.0).sum();
  let mut denom = Decom::new(1);
  for &(i, _) in &counts {
    denom = denom * fac(i);
  }
  let mut factor = Decom::new(1);
  for (_, i) in counts {
    factor = factor * i;
  }
  visited.remove(&n);
  if let Some(combo) = fac(total) * factor / denom {
    (total + 1, combo)
  } else {
    panic!("Failed to divide");
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: i64 = line.parse().unwrap();
  let mut tree = vec![vec![]; n as usize];
  for line in lines {
    let line = line.unwrap();
    let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
    let a = vs[0] - 1;
    let b = vs[1] - 1;
    tree[a as usize].push(b);
    tree[b as usize].push(a);
  }

  let mut visited = HashSet::new();
  for i in 0..n {
    let (_, combo) = dfs(&tree, &mut visited, i);
    println!("{}", combo % 1000000007)
  }
}