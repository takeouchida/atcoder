use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let l = lines.next().unwrap();
  let v: Vec<i32> = l.unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let x = v[0];
  let y = v[1];
  let mut nx = 0;
  let mut ny = 0;
  let mut nz = 0;
  let max = x + y;
  let mut scores = Vec::new();

  for _ in 0..3 {
    let l = lines.next().unwrap();
    let mut score: Vec<i32> = l.unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    score.sort();
    scores.push(score);
  }

  let mut total = 0;
  while nx + ny + nz < max {
    let mut options = Vec::new();
    if nx < x {
      options.push((scores[0][scores[0].len()-1], 0));
    }
    if ny < y {
      options.push((scores[1][scores[1].len()-1], 1));
    }
    if !scores[2].is_empty() {
      options.push((scores[2][scores[2].len()-1], 2));
    }
    options.sort_by(|x, y| y.0.cmp(&x.0));
    let (s, i) = options[0];
    total += s;
    if i == 0 {
      scores[0].pop();
      nx += 1;
    } else if i == 1 {
      scores[1].pop();
      ny += 1;
    } else {
      nz += 1;
      scores[2].pop();
    }
  }
  println!("{}", total);
}