use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let m = vs[1];
  let mut ac = vec![false; n];
  let mut penalty = vec![0; n];
  for _ in 0..m {
    let line = lines.next().unwrap().unwrap();
    let ss: Vec<&str> = line.split(' ').collect();
    let p: usize = ss[0].parse::<usize>().unwrap() - 1;
    if ss[1] == "AC" {
      ac[p] = true;
    } else if !ac[p] {
      penalty[p] += 1;
    }
  }
  let num_ac = ac.iter().filter(|x| **x).count();
  let num_penalty: usize = penalty.iter().sum();
  println!("{} {}", num_ac, num_penalty);
}