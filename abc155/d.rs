use std::io::{self, BufRead};
use std::collections::BinaryHeap;

fn find_comb(n: usize) -> usize {
  if n == 0 {
    0
  } else {
    n * (n - 1) / 2
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let k = vs[1] - 1;
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();

  let mut pos = Vec::new();
  let mut neg = Vec::new();
  let mut zeros = 0;

  for v in &vs {
    if *v > 0 {
      pos.push(*v);
    } else if *v < 0 {
      neg.push(-(*v));
    } else {
      zeros += 1;
    }
  }
  pos.sort();
  neg.sort();

  let count_pos = find_comb(pos.len()) + find_comb(neg.len());
  let count_zeros = find_comb(zeros) + zeros * (pos.len() + neg.len());
  let count_neg = pos.len() * neg.len();

  let ans = if k < count_neg as i64 {
    // negative
    let target = count_neg as i64 - 1 - k;
    let mut buf: BinaryHeap<(i64, usize, usize)> = BinaryHeap::new();
    for (i, p) in pos.iter().enumerate() {
      buf.push((-*p * neg[0], i, 0));
    }
    let mut i = 0;
    while i < target && !buf.is_empty() {
      let (v, j, k) = buf.pop().unwrap();
      if k < neg.len() - 1 {
        buf.push((-pos[j] * neg[k + 1], i as usize, k + 1));
      }
      i += 1;
    }
    let (v, _, _) = buf.pop().unwrap();
    v
  } else if k < (count_neg + count_zeros) as i64 {
    // zero
    0
  } else {
    // positive
    let target = k - (count_neg + count_zeros) as i64;
    let mut buf: BinaryHeap<(i64, usize, usize, bool)> = BinaryHeap::new();
    for i in (0..pos.len()-1) {
      buf.push((-pos[i] * pos[i+1], i, i+1, true));
    }
    for i in (0..neg.len()-1) {
      buf.push((-neg[i] * neg[i+1], i, i+1, false));
    }
    let mut i = 0;
    while i < target && !buf.is_empty() {
      let (v, j, k, is_pos) = buf.pop().unwrap();
      if is_pos {
        if k < pos.len() - 1 {
          buf.push((-pos[j] * pos[k + 1], j, k + 1, true))
        }
      } else {
        if k < neg.len() - 1 {
          buf.push((-neg[j] * neg[k + 1], j, k + 1, false))
        }
      }
      i += 1;
    }
    let (v, _, _, _) = buf.pop().unwrap();
    -v
  };

  println!("{}", ans);
}