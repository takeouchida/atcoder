use std::io::{self, BufRead};
use std::collections::BTreeMap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: i32 = line.parse().unwrap();
  let mut polls: BTreeMap<String, i32> = BTreeMap::new();
  for _ in 0..n {
    let line = lines.next().unwrap().unwrap();
    if polls.contains_key(&line) {
      let count = *polls.get(&line).unwrap();
      polls.insert(line, count + 1);
    } else {
      polls.insert(line, 1);
    }
  }
  let max_count = polls.values().max().unwrap();
  for (s, i) in polls.iter() {
    if *i == *max_count {
      println!("{}", s);
    }
  }
}