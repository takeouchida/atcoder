use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let vs: Vec<i32> = lines.next().unwrap().unwrap().as_bytes().iter().rev().map(|x| *x as i32 - 48).collect();
  let mut a0 = vs[0];
  let mut b0 = 10 - vs[0];
  for i in 1..vs.len() {
    let n = vs[i];
    let a1 = min(n + 1 + b0, n + a0);
    let b1 = min(9 - n + b0, 10 - n + a0);
    a0 = a1;
    b0 = b1;
  }
  println!("{}", min(a0, b0 + 1));
}