use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: i32 = line.parse().unwrap();
  if n % 3 != 0 {
    println!("-1");
    return;
  }
  for i in 0..n {
    for j in 0..n {
      let k = i / 3;
      let l = i % 3;
      let m = j / 3;
      let p = j % 3;
      if k == m {
        let c = match (l, p) {
          (0, 0) => 'a',
          (0, 1) => 'a',
          (0, 2) => 'b',
          (1, 0) => 'c',
          (1, 1) => '.',
          (1, 2) => 'b',
          (2, 0) => 'c',
          (2, 1) => 'd',
          (2, 2) => 'd',
          (_, _) => '.',
        };
        print!("{}", c);
      } else {
        print!(".")
      }
    }
    println!();
  }
}