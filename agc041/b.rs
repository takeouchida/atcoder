use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let xs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = xs[0];
  let m = xs[1];
  let v = xs[2];
  let p = xs[3];
  let line = lines.next().unwrap().unwrap();
  let mut xs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  xs.sort_by(|a, b| b.cmp(a));
  let mut xs: Vec<_> = xs.into_iter().enumerate().collect();
  println!("{:?}", xs);
  let mut border = xs[p-1].1;
  let mut q = p;
  while xs[q].1 == border {
    q += 1;
  }
  border = if border < v { 0 } else { border - v };
  while q < n && xs[q].1 >= border {
    q += 1;
  }
  println!("{}", q);
}