use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let a = vs[1];
  let b = vs[2];
  let d = b - a;
  let ans = if d % 2 == 0 {
    d / 2
  } else {
    min((n - b) + (n - a) + 1, a + b - 1) / 2
  };
  println!("{}", ans);
}