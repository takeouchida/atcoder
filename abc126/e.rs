use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn bfs(es: &Vec<Vec<usize>>, rest: &mut BTreeSet<usize>, n: usize) {
  rest.remove(&n);
  for &c in &es[n] {
    if rest.contains(&c) {
      bfs(es, rest, c);
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1]);
  let mut es: Vec<Vec<usize>> = vec![Vec::new(); n];
  for _ in 0..m {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (x, y) = (xs[0] - 1, xs[1] - 1);
    es[x].push(y);
    es[y].push(x);
  }
  let mut rest: BTreeSet<usize> = (0..n).collect();
  let mut count: usize = 0;
  while !rest.is_empty() {
    let n = rest.iter().next().unwrap().clone();
    bfs(&es, &mut rest, n);
    count += 1;
  }
  println!("{}", count);
}