use std::io::{self, BufRead};

fn dfs(es: &Vec<Vec<(usize, usize)>>, colors: &mut Vec<bool>, visited: &mut Vec<bool>, n: usize) {
  visited[n] = true;
  for &(u, w) in &es[n] {
    if !visited[u] {
      colors[u] = if w % 2 == 0 { colors[n] } else { !colors[n] };
      dfs(es, colors, visited, u);
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut es: Vec<Vec<(usize, usize)>> = vec![Vec::new(); n];
  for _ in 0..(n-1) {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (u, v, w) = (xs[0] - 1, xs[1] - 1, xs[2]);
    es[u].push((v, w));
    es[v].push((u, w));
  }
  let mut colors: Vec<bool> = vec![false; n];
  let mut visited: Vec<bool> = vec![false; n];
  dfs(&es, &mut colors, &mut visited, 0);
  for c in colors {
    println!("{}", if c { "1" } else { "0" });
  }
}