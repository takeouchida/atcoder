use std::io::{self, BufRead};

#[derive(Debug, Clone)]
struct Tree {
  size: usize,
  children: Vec<Tree>,
}

fn create_tree_(n: usize, es: &Vec<Vec<usize>>, visited: &mut Vec<bool>) -> (usize, Vec<Tree>) {
  visited[n] = true;
  let mut count = 1;
  let mut trees = Vec::new();
  for &x in &es[n] {
    if visited[x] {
      continue;
    }
    let (c, ts) = create_tree_(x, es, visited);
    count += c;
    for t in ts {
      trees.push(t);
    }
  }
  if count % 2 == 0 {
    (0, vec![Tree{size: count / 2, children: trees}])
  } else {
    (count, trees)
  }
}

fn create_tree(n: usize, es: &Vec<Vec<usize>>) -> Tree {
  let mut visited = vec![false; n];
  let (_, trees) = create_tree_(0, &es, &mut visited);
  trees[0].clone()
}

fn f(n: usize, t: &Tree) -> usize {
  let mut res = 1;
  for i in 0..t.size {
    res *= 2 * (i + n) + 1;
  }
  res = if t.children.is_empty() {
    res
  } else {
    res * (g(n + t.size, &t.children) + f(0, &t.children[0]) * g(n + t.size, &t.children[1..]))
  };
  println!("f {} {} {:?}", res, n, t);
  res
}

fn g(n: usize, t: &[Tree]) -> usize {
  if t.len() == 0 {
    return 0;
  }
  let mut res = 1;
  for i in 0..t[0].size {
    res *= 2 * (i + n) + 1;
  }
  res = f(0, &t[0]) + res * g(n + t[0].size, &t[1..]);
  println!("g {} {} {:?}", res, n, t);
  res
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut es: Vec<Vec<usize>> = vec![vec![]; n];
  for _ in 0..(n-1) {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (u, v) = (xs[0] - 1, xs[1] - 1);
    es[u].push(v);
    es[v].push(u);
  }
  let tree = create_tree(n, &es);
  println!("{}", f(0, &tree));
}