use std::io::{self, BufRead};

fn find(x: i64) -> Option<(i64, i64)> {
  for i in 1..200 {
    let i5 = i*i*i*i*i;
    for k in (-i+1)..(i+1) {
      let j = -k;
      let j5 = j*j*j*j*j;
      if i5 - j5 == x {
        return Some((i, j));
      }
    }
  }
  None
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let x: i64 = lines.next().unwrap().unwrap().parse().unwrap();
  if let Some((i, j)) = find(x) {
    println!("{} {}", i, j);
  }
}