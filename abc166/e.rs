use std::io::{self, BufRead};

pub fn lower_bound(vec: &[(usize, usize, i32)], val: i32) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p].2 < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

pub fn upper_bound(vec: &[(usize, usize, i32)], val: i32) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p].2 > val {
      hi = p;
    } else {
      lo = p + 1;
    }
  }
  return lo;
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut xs: Vec<(usize, usize, i32)> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).enumerate().map(|(i, h)| (i, h, i as i32 - h as i32)).collect();
  xs.sort_by(|a, b| (a.2).cmp(&(b.2)));
  let mut ans = 0;
  for i in 0..n {
    let d = (xs[i].0 + xs[i].1) as i32;
    let lo = lower_bound(&xs, d);
    let hi = upper_bound(&xs, d);
    ans += hi - lo;
  }
  println!("{}", ans);
}