use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1]);
  let hs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut es: Vec<Vec<usize>> = vec![vec![]; n];
  for _ in 0..m {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (a, b) = (xs[0] - 1, xs[1] - 1);
    es[a].push(b);
    es[b].push(a);
  }
  let mut count: usize = 0;
  for i in 0..n {
    let h = hs[i];
    let mut b = true;
    for j in &es[i] {
      if h <= hs[*j] {
        b = false;
      }
    }
    if b {
      count += 1;
    }
  }
  println!("{}", count);
}