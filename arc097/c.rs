use std::io::{self, BufRead};
use std::cmp::min;
use std::collections::BTreeSet;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let s = lines.next().unwrap().unwrap();
  let k: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut ss: BTreeSet<&str> = BTreeSet::new();
  for i in 0..s.len() {
    let len = min(5, s.len() - i);
    for j in 0..len {
      ss.insert(&s[i..(i+j+1)]);
    }
  }
  let mut it = ss.iter();
  for _ in 0..(k-1) {
    it.next();
  }
  println!("{}", it.next().unwrap());
}