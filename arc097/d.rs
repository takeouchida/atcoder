use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn dfs(es: &Vec<Vec<usize>>, rest: &mut BTreeSet<usize>, visited: &mut BTreeSet<usize>, n: usize) {
  visited.insert(n);
  rest.remove(&n);
  for i in &es[n] {
    if visited.contains(i) {
      continue;
    }
    dfs(es, rest, visited, *i);
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1]);
  let ps: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse::<usize>().unwrap() - 1).collect();
  let mut es: Vec<Vec<usize>> = vec![vec![]; n];
  for _ in 0..m {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (x, y) = (xs[0] - 1, xs[1] - 1);
    es[x].push(y);
    es[y].push(x);
  }
  let mut rest: BTreeSet<usize> = (0..n).collect();
  let mut count: usize = 0;
  while !rest.is_empty() {
    let n = *rest.iter().next().unwrap();
    let mut visited: BTreeSet<usize> = BTreeSet::new();
    dfs(&es, &mut rest, &mut visited, n);
    let vs: BTreeSet<usize> = visited.iter().map(|v| ps[*v]).collect();
    count += visited.iter().filter(|v| vs.contains(v)).count();
  }
  println!("{}", count);
}