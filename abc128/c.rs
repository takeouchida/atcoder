use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1]);
  let mut ss: Vec<Vec<usize>> = Vec::new();
  for _ in 0..m {
    let mut xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse::<usize>().unwrap() - 1).collect();
    xs.remove(0);
    ss.push(xs);
  }
  let ps: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let count: usize = (2 as usize).pow(n as u32);
  let mut ans = 0;
  for i in 0..count {
    let mut ok = true;
    for j in 0..m {
      let parity = ss[j].iter().filter(|s| ((1 as usize) << **s) & i != 0).count() % 2;
      if parity != ps[j] {
        ok = false;
        break;
      }
    }
    if ok {
      ans += 1;
    }
  }
  println!("{}", ans);
}