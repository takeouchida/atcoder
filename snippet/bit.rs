struct BIT {
  tree: Vec<usize>
}

impl BIT {
  pub fn new(n: usize) -> BIT {
    BIT { tree: vec![0; n + 1] }
  }

  pub fn sum(&self, i: usize) -> usize {
    assert!(i < self.tree.len());
    let mut j = i as isize;
    let mut s: usize = 0;
    while j > 0 {
      s += self.tree[j as usize];
      j -= j & -j;
    }
    s
  }

  pub fn add(&mut self, i: usize, x: usize) {
    assert!(i < self.tree.len());
    let mut j = i as isize;
    while j < self.tree.len() as isize {
      self.tree[j as usize] += x;
      j += j & -j;
    }
  }
}

#[test]
fn test_1() {
  let xs: Vec<usize> = vec![1, 1, 1, 2, 2, 3];
  let mut bit = BIT::new(4);
  for &x in &xs {
    bit.add(x, 1);
  }
  assert_eq!(3, bit.sum(1));
  assert_eq!(5, bit.sum(2));
  assert_eq!(6, bit.sum(3));
  assert_eq!(6, bit.sum(4));
}

#[test]
fn test_2() {
  let bit = BIT::new(4);
  assert_eq!(0, bit.sum(1));
  assert_eq!(0, bit.sum(2));
  assert_eq!(0, bit.sum(3));
  assert_eq!(0, bit.sum(4));
}