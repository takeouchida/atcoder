use std::collections::HashMap;

pub fn is_prime(n: i64) -> bool {
  let mut i = 2;
  while i * i <= n {
    if n % i == 0 {
      return false
    }
    i += 1
  }
  n != 1
}

pub fn find_divisors(n: i64) -> Vec<i64> {
  let mut res = Vec::new();
  let mut i = 1;
  while i * i <= n {
    if n % i == 0 {
      res.push(i);
      if i != n / i {
        res.push(n / i);
      }
    }
    i += 1
  }
  res
}

pub fn find_prime_factors(n: i64) -> HashMap<i64, i64> {
  let mut res = HashMap::new();
  let mut i = 2;
  let mut m = n;
  while i * i <= m {
    while m % i == 0 {
      let val = 1 + res.get(&i).unwrap_or(&0);
      res.insert(i, val);
      m /= i;
    }
    i += 1
  }
  if m != 1 {
    let val = 1 + res.get(&m).unwrap_or(&0);
    res.insert(m, val);
  }
  res
}

#[test]
fn test_is_prime() {
  let expect = vec![false, true, true, false, true, false, true, false, false, false, true, false, true, false, false, false, true];
  for i in 0..expect.len() {
    let n = (i + 1) as i64;
    assert_eq!(expect[i], is_prime(n))
  }
}

#[test]
fn test_find_divisors() {
  assert_eq!(vec![1], find_divisors(1));
  assert_eq!(vec![1, 2], find_divisors(2));
  assert_eq!(vec![1, 3], find_divisors(3));
  assert_eq!(vec![1, 4, 2], find_divisors(4));
  assert_eq!(vec![1, 5], find_divisors(5));
  assert_eq!(vec![1, 6, 2, 3], find_divisors(6));
  assert_eq!(vec![1, 7], find_divisors(7));
  assert_eq!(vec![1, 8, 2, 4], find_divisors(8));
  assert_eq!(vec![1, 9, 3], find_divisors(9));
}

#[test]
fn test_find_prime_factors() {
  let ps2 = find_prime_factors(2);
  assert_eq!(1, ps2.len());
  assert_eq!(Some(&1), ps2.get(&2));

  let ps3 = find_prime_factors(3);
  assert_eq!(1, ps3.len());
  assert_eq!(Some(&1), ps3.get(&3));

  let ps4 = find_prime_factors(4);
  assert_eq!(1, ps4.len());
  assert_eq!(Some(&2), ps4.get(&2));

  let ps6 = find_prime_factors(6);
  assert_eq!(2, ps6.len());
  assert_eq!(Some(&1), ps6.get(&2));
  assert_eq!(Some(&1), ps6.get(&3));

  let ps8 = find_prime_factors(8);
  assert_eq!(1, ps8.len());
  assert_eq!(Some(&3), ps8.get(&2));

  let ps24 = find_prime_factors(24);
  assert_eq!(2, ps24.len());
  assert_eq!(Some(&3), ps24.get(&2));
  assert_eq!(Some(&1), ps24.get(&3));

  let ps60 = find_prime_factors(60);
  assert_eq!(3, ps60.len());
  assert_eq!(Some(&2), ps60.get(&2));
  assert_eq!(Some(&1), ps60.get(&3));
  assert_eq!(Some(&1), ps60.get(&5));
}