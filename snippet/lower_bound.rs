pub fn lower_bound<T: Ord>(vec: &[T], val: T) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

#[test]
fn test_lower_bound_1() {
  let v: Vec<i32> = vec![];
  let i = lower_bound(&v, 1);
  assert_eq!(i, 0);
}

#[test]
fn test_lower_bound_2() {
  let v: Vec<i32> = vec![1];
  let i = lower_bound(&v, 0);
  assert_eq!(i, 0);
}

#[test]
fn test_lower_bound_3() {
  let v: Vec<i32> = vec![1];
  let i = lower_bound(&v, 1);
  assert_eq!(i, 0);
}

#[test]
fn test_lower_bound_4() {
  let v: Vec<i32> = vec![1];
  let i = lower_bound(&v, 2);
  assert_eq!(i, 1);
}

#[test]
fn test_lower_bound_5() {
  let v: Vec<i32> = vec![0, 1];
  let i = lower_bound(&v, -1);
  assert_eq!(i, 0);
}

#[test]
fn test_lower_bound_6() {
  let v: Vec<i32> = vec![0, 1];
  let i = lower_bound(&v, 0);
  assert_eq!(i, 0);
}

#[test]
fn test_lower_bound_7() {
  let v: Vec<i32> = vec![0, 1];
  let i = lower_bound(&v, 1);
  assert_eq!(i, 1);
}

#[test]
fn test_lower_bound_8() {
  let v: Vec<i32> = vec![0, 1];
  let i = lower_bound(&v, 2);
  assert_eq!(i, 2);
}

#[test]
fn test_lower_bound_9() {
  let v: Vec<i32> = vec![1, 1];
  let i = lower_bound(&v, 0);
  assert_eq!(i, 0);
}

#[test]
fn test_lower_bound_10() {
  let v: Vec<i32> = vec![1, 1];
  let i = lower_bound(&v, 1);
  assert_eq!(i, 0);
}

#[test]
fn test_lower_bound_11() {
  let v: Vec<i32> = vec![1, 1];
  let i = lower_bound(&v, 2);
  assert_eq!(i, 2);
}

#[test]
fn test_lower_bound_12() {
  let v: Vec<i32> = vec![0, 1, 2];
  let i = lower_bound(&v, -1);
  assert_eq!(i, 0);
}

#[test]
fn test_lower_bound_13() {
  let v: Vec<i32> = vec![0, 1, 2];
  let i = lower_bound(&v, 0);
  assert_eq!(i, 0);
}

#[test]
fn test_lower_bound_14() {
  let v: Vec<i32> = vec![0, 1, 2];
  let i = lower_bound(&v, 1);
  assert_eq!(i, 1);
}

#[test]
fn test_lower_bound_15() {
  let v: Vec<i32> = vec![0, 1, 2];
  let i = lower_bound(&v, 2);
  assert_eq!(i, 2);
}

#[test]
fn test_lower_bound_16() {
  let v: Vec<i32> = vec![0, 1, 2];
  let i = lower_bound(&v, 3);
  assert_eq!(i, 3);
}

#[test]
fn test_lower_bound_17() {
  let v: Vec<i32> = vec![0, 1, 1, 2];
  let i = lower_bound(&v, 1);
  assert_eq!(i, 1);
}

#[test]
fn test_lower_bound_19() {
  let v: Vec<i32> = vec![0, 1, 1, 2];
  let i = lower_bound(&v, 2);
  assert_eq!(i, 3);
}

#[test]
fn test_lower_bound_20() {
  let v: Vec<i32> = vec![0, 1, 1, 2];
  let i = lower_bound(&v, 3);
  assert_eq!(i, 4);
}

#[test]
fn test_lower_bound_21() {
  let v: Vec<i32> = vec![0, 0, 1, 1, 2];
  let i = lower_bound(&v, 0);
  assert_eq!(i, 0);
}

#[test]
fn test_lower_bound_22() {
  let v: Vec<i32> = vec![0, 0, 1, 1, 2];
  let i = lower_bound(&v, 1);
  assert_eq!(i, 2);
}

#[test]
fn test_lower_bound_23() {
  let v: Vec<i32> = vec![0, 0, 1, 1, 2];
  let i = lower_bound(&v, 4);
  assert_eq!(i, 5);
}


#[test]
fn test_lower_bound_24() {
  let v: Vec<i32> = vec![0, 0, 1, 2, 2];
  let i = lower_bound(&v, 2);
  assert_eq!(i, 3);
}
#[test]
fn test_lower_bound_25() {
  let v: Vec<i32> = vec![0, 0, 1, 2, 2];
  let i = lower_bound(&v, 4);
  assert_eq!(i, 5);
}