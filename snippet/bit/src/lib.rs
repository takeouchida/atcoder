struct BIT {
  tree: Vec<usize>
}

impl BIT {
  pub fn new(n: usize) -> BIT {
    BIT { tree: vec![0; n + 1] }
  }

  pub fn sum(&self, i: usize) -> usize {
    assert!(i < self.tree.len());
    let mut j = i as isize;
    let mut s: usize = 0;
    while j > 0 {
      s += self.tree[j as usize];
      j -= j & -j;
    }
    s
  }

  pub fn add(&mut self, i: usize, x: usize) {
    assert!(i < self.tree.len());
    let mut j = i as isize;
    while j < self.tree.len() as isize {
      self.tree[j as usize] += x;
      j += j & -j;
    }
  }
}

struct BIT2 {
  tree: Vec<Vec<usize>>
}

impl BIT2 {
  pub fn new(w: usize, h: usize) -> BIT2 {
    assert!(w > 0);
    assert!(h > 0);
    BIT2 { tree: vec![vec![0; w + 1]; h + 1] }
  }

  pub fn sum(&self, i: usize, j: usize) -> usize {
    assert!(i < self.tree[0].len());
    assert!(j < self.tree.len());
    let mut k = i as isize;
    let mut s: usize = 0;
    while k > 0 {
      let mut l = j as isize;
      while l > 0 {
        s += self.tree[l as usize][k as usize];
        l -= l & -l;
      }
      k -= k & -k;
    }
    s
  }

  pub fn add(&mut self, i: usize, j: usize, x: usize) {
    assert!(i < self.tree[0].len());
    assert!(j < self.tree.len());
    let mut k = i as isize;
    while k < self.tree[0].len() as isize {
      let mut l = j as isize;
      while l < self.tree.len() as isize {
        self.tree[l as usize][k as usize] += x;
        l += l & -l;
      }
      k += k & -k;
    }
  }
}

#[cfg(test)]
mod tests {
  use super::{BIT, BIT2};
  use rand::prelude::*;

  #[test]
  fn test_bit_1() {
    let xs: Vec<usize> = vec![1, 1, 1, 2, 2, 3];
    let mut bit = BIT::new(4);
    for &x in &xs {
      bit.add(x, 1);
    }
    assert_eq!(3, bit.sum(1));
    assert_eq!(5, bit.sum(2));
    assert_eq!(6, bit.sum(3));
    assert_eq!(6, bit.sum(4));
  }

  #[test]
  fn test_bit_2() {
    let bit = BIT::new(4);
    assert_eq!(0, bit.sum(1));
    assert_eq!(0, bit.sum(2));
    assert_eq!(0, bit.sum(3));
    assert_eq!(0, bit.sum(4));
  }

  #[test]
  fn test_bit_3() {
    let mut bit = BIT::new(1000);
    let mut xs: Vec<usize> = Vec::new();
    let mut rng = rand::thread_rng();
    let n: usize = 1000;
    for _ in 0..n {
      let i = (rng.gen::<f64>() * 999.0) as usize + 1;
      bit.add(i, 1);
      xs.push(i);
    }
    let mut count: usize = 0;
    for i in 1..n {
      count += xs.iter().filter(|x| **x == i).count();
      assert_eq!(count, bit.sum(i));
    }
  }

  #[test]
  fn test_bit2_1() {
    let xs: Vec<(usize, usize)> = vec![(1, 2), (2, 4), (3, 1), (4, 3)];
    let mut bit = BIT2::new(4, 4);
    for &(x, y) in &xs {
      bit.add(x, y, 1);
    }
    assert_eq!(0, bit.sum(1, 1));
    assert_eq!(1, bit.sum(1, 2));
    assert_eq!(1, bit.sum(1, 3));
    assert_eq!(1, bit.sum(1, 4));
    assert_eq!(0, bit.sum(2, 1));
    assert_eq!(1, bit.sum(2, 2));
    assert_eq!(1, bit.sum(2, 3));
    assert_eq!(2, bit.sum(2, 4));
    assert_eq!(1, bit.sum(3, 1));
    assert_eq!(2, bit.sum(3, 2));
    assert_eq!(2, bit.sum(3, 3));
    assert_eq!(3, bit.sum(3, 4));
    assert_eq!(1, bit.sum(4, 1));
    assert_eq!(2, bit.sum(4, 2));
    assert_eq!(3, bit.sum(4, 3));
    assert_eq!(4, bit.sum(4, 4));
  }

  #[test]
  fn test_bit2_2() {
    let n: usize = 100;
    let mut bit = BIT2::new(n, n);
    let mut xs: Vec<(usize, usize)> = Vec::new();
    let mut rng = rand::thread_rng();
    for _ in 0..n {
      let i = (rng.gen::<f64>() * 99.0) as usize + 1;
      let j = (rng.gen::<f64>() * 99.0) as usize + 1;
      bit.add(i, j, 1);
      xs.push((i, j));
    }
    for i in 1..n {
      for j in 1..n {
        let count = xs.iter().filter(|(x, y)| *x <= i && *y <= j).count();
        assert_eq!(count, bit.sum(i, j));
      }
    }
  }
}
