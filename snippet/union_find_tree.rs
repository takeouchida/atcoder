struct Node {
  parent: usize,
  rank: usize,
}

struct UnionFindTree {
  nodes: Vec<Node>,
}

impl UnionFindTree {
  pub fn new(n: usize) -> Self {
    let mut t = Vec::with_capacity(n);
    for i in 0..n {
      t.push(Node{parent: i, rank: 0});
    }
    UnionFindTree{nodes: t}
  }

  pub fn find_root(&mut self, x: usize) -> usize {
    assert!(x < self.nodes.len());
    let mut r = x;
    while self.nodes[r].parent != r {
      r = self.nodes[r].parent;
    }
    let mut i = r;
    while self.nodes[i].parent != r {
      self.nodes[i].parent = r;
      i = self.nodes[r].parent;
    }
    r
  }

  pub fn unite(&mut self, x: usize, y: usize) {
    assert!(x < self.nodes.len());
    assert!(y < self.nodes.len());
    let rx = self.find_root(x);
    let ry = self.find_root(y);
    if rx == ry {
      return
    }
    if self.nodes[rx].rank < self.nodes[ry].rank {
      self.nodes[rx].parent = ry;
    } else {
      self.nodes[ry].parent = rx;
      if self.nodes[rx].rank == self.nodes[ry].rank {
        self.nodes[x].rank += 1;
      }
    }
  }

  pub fn same(&mut self, x: usize, y: usize) -> bool {
    return self.find_root(x) == self.find_root(y)
  }
}

#[test]
fn test_1() {
  let mut t = UnionFindTree::new(2);
  assert_eq!(false, t.same(0, 1));
  t.unite(0, 1);
  assert_eq!(true, t.same(0, 1));
}

#[test]
fn test_2() {
  let mut t = UnionFindTree::new(3);
  assert_eq!(false, t.same(0, 1));
  assert_eq!(false, t.same(1, 2));
  t.unite(0, 1);
  assert_eq!(true, t.same(0, 1));
  assert_eq!(false, t.same(0, 2));
  assert_eq!(false, t.same(1, 2));
  t.unite(1, 2);
  assert_eq!(true, t.same(0, 1));
  assert_eq!(true, t.same(0, 2));
  assert_eq!(true, t.same(1, 2));
}

#[test]
fn test_3() {
  let mut t = UnionFindTree::new(4);
  t.unite(0, 1);
  assert_eq!(true, t.same(0, 1));
  assert_eq!(false, t.same(2, 3));
  assert_eq!(false, t.same(1, 2));
  assert_eq!(false, t.same(0, 3));
  t.unite(0, 2);
  assert_eq!(true, t.same(1, 2));
  assert_eq!(false, t.same(2, 3));
  assert_eq!(false, t.same(1, 3));
  t.unite(2, 3);
  assert_eq!(true, t.same(0, 1));
  assert_eq!(true, t.same(0, 2));
  assert_eq!(true, t.same(1, 2));
  assert_eq!(true, t.same(2, 3));
}