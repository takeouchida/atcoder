struct BIT2 {
  tree: Vec<Vec<usize>>
}

impl BIT2 {
  pub fn new(w: usize, h: usize) -> BIT2 {
    assert!(w > 0);
    assert!(h > 0);
    BIT2 { tree: vec![vec![0; w + 1]; h + 1] }
  }

  pub fn sum(&self, i: usize, j: usize) -> usize {
    assert!(i < self.tree[0].len());
    assert!(j < self.tree.len());
    let mut k = i as isize;
    let mut s: usize = 0;
    while k > 0 {
      let mut l = j as isize;
      while l > 0 {
        s += self.tree[l as usize][k as usize];
        l -= l & -l;
      }
      k -= k & -k;
    }
    s
  }

  pub fn add(&mut self, i: usize, j: usize, x: usize) {
    assert!(i < self.tree[0].len());
    assert!(j < self.tree.len());
    println!("add {} {} {}", i, j, x);
    let mut k = i as isize;
    while k < self.tree[0].len() as isize {
      let mut l = j as isize;
      while l < self.tree.len() as isize {
        println!("  {} {}", k, l);
        self.tree[l as usize][k as usize] += x;
        l += l & -l;
      }
      k += k & -k;
    }
  }
}

fn main() {
  let xs: Vec<(usize, usize)> = vec![(1, 2), (2, 4), (3, 1), (4, 3)];
  let mut bit = BIT2::new(4, 4);
  for &(x, y) in &xs {
    bit.add(x, y, 1);
  }
  for i in 1..=4 {
    for j in 1..=4 {
      println!("{} {} {}", i, j, bit.sum(i, j));
    }
  }
}