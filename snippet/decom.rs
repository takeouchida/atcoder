use std::collections::HashMap;
use std::ops::{Mul, Div, Rem};
use std::cmp::Ordering;

fn find_prime_factors(n: i64) -> HashMap<i64, i64> {
  let mut res = HashMap::new();
  let mut i = 2;
  let mut m = n;
  while i * i <= m {
    while m % i == 0 {
      let val = 1 + res.get(&i).unwrap_or(&0);
      res.insert(i, val);
      m /= i;
    }
    i += 1
  }
  if m != 1 {
    let val = 1 + res.get(&m).unwrap_or(&0);
    res.insert(m, val);
  }
  res
}

struct Decom {
  fs: HashMap<i64, i64>
}

impl Decom {
  pub fn new(n: i64) -> Self {
    Decom { fs: find_prime_factors(n) }
  }

  pub fn to_i64(self) -> i64 {
    let mut result = 1;
    for (k, v) in self.fs.iter() {
      for _ in 0..*v {
        result *= k;
      }
    }
    result
  }
}

impl Mul for Decom {
  type Output = Self;

  fn mul(self, rhs: Self) -> Self {
    let mut result = self.fs.clone();
    for (k, v) in rhs.fs.iter() {
      match self.fs.get(k) {
        None    => result.insert(*k, *v),
        Some(n) => result.insert(*k, n + *v),
      };
    }
    Decom { fs: result }
  }
}

impl Div for Decom {
  type Output = Option<Self>;

  fn div(self, rhs: Self) -> Option<Self> {
    let mut result = self.fs.clone();
    for (k, v) in rhs.fs.iter() {
      match self.fs.get(k) {
        None => return None,
        Some(n) => match n.cmp(v) {
          Ordering::Less => { return None; },
          Ordering::Equal => { result.remove(k); },
          Ordering::Greater => { result.insert(*k, *n - v); },
        },
      }
    }
    Some(Decom{ fs: result })
  }
}

impl Rem<i64> for Decom {
  type Output = i64;

  fn rem(self, rhs: i64) -> i64 {
    let mut result = 1;
    for (k, v) in self.fs.iter() {
      for _ in 0..*v {
        result = (result * *k) % rhs
      }
    }
    result
  }
}

#[test]
fn test_mul_1() {
  let x = Decom::new(2);
  let y = Decom::new(3);
  let z = x * y;

  assert_eq!(2, z.fs.len());
  assert_eq!(Some(&1), z.fs.get(&2));
  assert_eq!(Some(&1), z.fs.get(&3));
}

#[test]
fn test_mul_2() {
  let x = Decom::new(1);
  let y = Decom::new(1);
  let z = x * y;

  assert_eq!(0, z.fs.len());
}

#[test]
fn test_mul_3() {
  let x = Decom::new(1);
  let y = Decom::new(6);
  let z = x * y;

  assert_eq!(2, z.fs.len());
  assert_eq!(Some(&1), z.fs.get(&2));
  assert_eq!(Some(&1), z.fs.get(&3));
}

#[test]
fn test_mul_4() {
  let x = Decom::new(6);
  let y = Decom::new(1);
  let z = x * y;

  assert_eq!(2, z.fs.len());
  assert_eq!(Some(&1), z.fs.get(&2));
  assert_eq!(Some(&1), z.fs.get(&3));
}

#[test]
fn test_mul_5() {
  let x = Decom::new(15);
  let y = Decom::new(6);
  let z = x * y;

  assert_eq!(3, z.fs.len());
  assert_eq!(Some(&1), z.fs.get(&2));
  assert_eq!(Some(&2), z.fs.get(&3));
  assert_eq!(Some(&1), z.fs.get(&5));
}

#[test]
fn test_div_1() {
  let x = Decom::new(1);
  let y = Decom::new(1);

  if let Some(z) = x / y {
    assert_eq!(0, z.fs.len());
  } else {
    assert!(false);
  }
}

#[test]
fn test_div_2() {
  let x = Decom::new(2);
  let y = Decom::new(1);

  if let Some(z) = x / y {
    assert_eq!(1, z.fs.len());
    assert_eq!(Some(&1), z.fs.get(&2));
  } else {
    assert!(false);
  }
}

#[test]
fn test_div_3() {
  let x = Decom::new(6);
  let y = Decom::new(2);

  if let Some(z) = x / y {
    assert_eq!(1, z.fs.len());
    assert_eq!(Some(&1), z.fs.get(&3));
  } else {
    assert!(false);
  }
}

#[test]
fn test_div_4() {
  let x = Decom::new(24);
  let y = Decom::new(6);

  if let Some(z) = x / y {
    assert_eq!(1, z.fs.len());
    assert_eq!(Some(&2), z.fs.get(&2));
  } else {
    assert!(false);
  }
}

#[test]
fn test_div_5() {
  let x = Decom::new(60);
  let y = Decom::new(6);

  if let Some(z) = x / y {
    assert_eq!(2, z.fs.len());
    assert_eq!(Some(&1), z.fs.get(&2));
    assert_eq!(Some(&1), z.fs.get(&5));
  } else {
    assert!(false);
  }
}

#[test]
fn test_div_6() {
  let x = Decom::new(1);
  let y = Decom::new(2);

  if let Some(_) = x / y {
    assert!(false);
  }
}

#[test]
fn test_div_7() {
  let x = Decom::new(10);
  let y = Decom::new(3);

  if let Some(_) = x / y {
    assert!(false);
  }
}

#[test]
fn test_div_8() {
  let x = Decom::new(12);
  let y = Decom::new(9);

  if let Some(_) = x / y {
    assert!(false);
  }
}

#[test]
fn test_fac() {
  let mut x = Decom::new(1);
  for i in 2..=10 {
    x = x * Decom::new(i);
  }
  let mut y = Decom::new(1);
  for i in 2..=9 {
    y = y * Decom::new(i);
  }

  if let Some(z) = x / y {
    assert_eq!(2, z.fs.len());
    assert_eq!(Some(&1), z.fs.get(&2));
    assert_eq!(Some(&1), z.fs.get(&5));
  } else {
    assert!(false);
  }
}

#[test]
fn test_mod_1() {
  let x = Decom::new(1);
  assert_eq!(1, x % 2);

  let x = Decom::new(2);
  assert_eq!(2, x % 3);

  let x = Decom::new(6);
  assert_eq!(0, x % 2);

  let x = Decom::new(6);
  assert_eq!(0, x % 3);

  let x = Decom::new(24);
  assert_eq!(24, x % 25);

  let x = Decom::new(120);
  assert_eq!(20, x % 25);
}