use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: u64 = lines.next().unwrap().unwrap().parse().unwrap();
  let mut ans: u64 = 0;
  for i in 1..=n {
    let count = n / i;
    let end = i * count;
    ans += (i + end) * count / 2;
  }
  println!("{}", ans);
}