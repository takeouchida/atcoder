use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m, k) = (xs[0], xs[1], xs[2]);
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let ys: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut zs: Vec<usize> = vec![0; n + 1];
  let mut ws: Vec<usize> = vec![0; m + 1];
  for i in 0..n {
    zs[i+1] = zs[i] + xs[i];
  }
  for i in 0..m {
    ws[i+1] = ws[i] + ys[i];
  }
  let mut i = zs.iter().map(|&z| z <= k).count() - 1;
  let l = ws.iter().map(|&w| w <= k).count();
  let mut j: usize = 0;
  let mut ans: usize = 0;
  while j < l {
    if zs[i] + ws[j] <= k {
      ans = max(ans, i + j);
      j += 1;
    } else if i > 0 {
      i -= 1;
    } else {
      break;
    }
  }
  println!("{}", ans);
}