use std::io::{self, BufRead};

fn pow(n: usize, p: usize, m: usize) -> usize {
  let mut q = p;
  let mut r = n;
  let mut result = 1;
  while q > 0 {
    if q % 2 == 1 {
      result = result * r % m;
    }
    r = r * r % m;
    q >>= 1;
  }
  result
}

fn div(n: usize, m: usize, p: usize) -> usize {
  n * pow(m, p - 2, p) % p
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let p: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let zs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut xss: Vec<Vec<usize>> = vec![vec![0; p + 1]; p];
  xss[0][0] = 1;
  xss[0][p] = zs[0];
  for i in 1..p {
    let mut x = 1;
    for j in 0..p {
      xss[i][j] = x;
      x = x * i % p;
    }
    xss[i][p] = zs[i];
  }
  for i in 0..p {
    if xss[i][i] != 1 {
      let d = xss[i][i];
      for j in i..(p+1) {
        xss[i][j] = div(xss[i][j], d, p);
      }
    }
    for j in (i+1)..p {
      let m = xss[j][i];
      for k in i..(p+1) {
        xss[j][k] = (xss[j][k] + (p - (m * xss[i][k] % p))) % p;
      }
    }
  }
  for i in 0..p {
    let k = p - i - 1;
    for j in (i+1)..p {
      let l = p - j - 1;
      let m = xss[l][k];
      for q in k..(p+1) {
        xss[l][q] = (xss[l][q] + (p - (m * xss[k][q] % p))) % p;
      }
    }
  }
  for i in 0..p {
    print!("{} ", xss[i][p]);
  }
  println!();
}