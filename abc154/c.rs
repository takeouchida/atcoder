use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let _ = lines.next().unwrap().unwrap();
  let line = lines.next().unwrap().unwrap();
  let mut vs: Vec<i32> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  vs.sort();
  for i in 0..(vs.len()-1) {
    if vs[i] == vs[i+1] {
      println!("NO");
      return;
    }
  }
  println!("YES");
}