use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let k = vs[1];
  let line = lines.next().unwrap().unwrap();
  let ps: Vec<f64> = line.split(' ').map(|x| x.parse::<f64>().unwrap()).map(|p| (1.0 + p) / 2.0).collect();
  let mut ex: f64 = (0..k).fold(0.0, |acc, i| acc + ps[i]);
  let mut max = ex;
  for i in 0..(n-k) {
    ex = ex + ps[k+i] - ps[i];
    if max < ex {
      max = ex;
    }
  }
  println!("{}", max);
}