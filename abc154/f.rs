use std::io::{self, BufRead};

const M: i64 = 1000000007;

fn fac(n: usize) -> i64 {
  let mut result: i64 = 1;
  let mut m = n;
  while m > 1 {
    result = result * m as i64 % M;
    m -= 1;
  }
  result
}

fn pow(n: i64, p: i64) -> i64 {
  let mut q = p;
  let mut r = n;
  let mut result = 1;
  while q > 0 {
    if q % 2 == 1 {
      result = result * r % M;
    }
    r = r * r % M;
    q >>= 1;
  }
  result
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (r1, c1, r2, c2) = (xs[0], xs[1], xs[2], xs[3]);
  let mut ps: Vec<i64> = vec![0; r2 - r1 + 1];
  let mut qs: Vec<i64> = vec![0; c2 - c1 + 1];
  let mut rs: Vec<i64> = vec![0; r2 - r1 + c2 - c1 + 3];
  let mut temp = 1;
  for i in 0..(r2-r1+1) {
    let l = ps.len();
    ps[l-i-1] = temp;
    temp = (temp * (r2 - i) as i64) % M;
  }
  let mut temp = 1;
  for i in 0..(c2-c1+1) {
    let l = qs.len();
    qs[l-i-1] = temp;
    temp = (temp * (c2 - i) as i64) % M;
  }
  let mut temp = 1;
  for i in 0..(r2-r1+c2-c1+3) {
    rs[i] = temp;
    temp = (temp * (r1 + c1 + i + 1) as i64) % M;
  }
  let coeff = fac(r1 + c1) * pow(fac(r2) * fac(c2) % M, M - 2) % M;
  let mut result: i64 = 0;
  for i in 0..(r2-r1+1) {
    for j in 0..(c2-c1+1) {
      result = (result + (ps[i] * qs[j] % M) * rs[i+j] % M) % M;
    }
  }
  result = result * coeff % M;
  println!("{}", result);
}