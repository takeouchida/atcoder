use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: Vec<i32> = lines.next().unwrap().unwrap().chars().rev().map(|x| x as i32 - 48).collect();
  let line = lines.next().unwrap().unwrap();
  let k: usize = line.parse().unwrap();
  let digit = n.len();

  let mut buf = vec![vec![0; k+1]; digit-1];
  buf[0][0] = 1;
  buf[0][1] = 9;
  for i in 1..(digit-1) {
    buf[i][0] = 1;
    for j in 1..k {
      buf[i][j] = buf[i-1][j-1] * 9 + buf[i-1][j];
    }
    buf[i][k] = buf[i-1][k-1] * 9;
  }
  println!("{:?}", buf);
  println!("{:?}", (0..(digit-1)).map(|i| buf[i][k]).collect::<Vec<i32>>());
  println!("{:?}", (0..k).map(|i| buf[digit-2-i][k-1-i] * (n[digit-1-i] - 1)).collect::<Vec<i32>>());
  println!("{:?}", (0..k).map(|i| buf[digit-3-i][k-1-i]).collect::<Vec<i32>>());
  // let ans: i32 = (0..(digit-1)).map(|i| buf[i][k]).sum::<i32>() + (1..k).map(|i| buf[digit-1-i][k-1-i]).sum::<i32>();
  // println!("{}", ans);
}