use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, k) = (xs[0], xs[1]);
  let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut ys: Vec<u64> = vec![0; xs.len() + 1];
  ys[0] = 0;
  for i in 0..xs.len() {
    ys[i+1] = ys[i] + xs[i];
  }
  let mut j: usize = 0;
  let mut count: usize = 0;
  for i in 0..xs.len() {
    while j < xs.len() && ys[j+1] - ys[i] < k as u64 {
      j += 1;
    }
    count += j - i;
  }
  count = n * (n + 1) / 2 - count;
  println!("{}", count);
}