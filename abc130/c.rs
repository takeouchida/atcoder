use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (w, h, x, y) = (xs[0], xs[1], xs[2], xs[3]);
  let s: f64 = (w * h) as f64 / 2.0;
  let ok = (w % 2 == 0 && h % 2 == 0 && 2 * x == w && 2 * y == h) as i32;
  println!("{} {}", s, ok);
}