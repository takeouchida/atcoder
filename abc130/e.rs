use std::io::{self, BufRead};

const M: u64 = 1000000007;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1]);
  let ss: Vec<u32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let ts: Vec<u32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut memo1: Vec<Vec<u64>> = vec![vec![0; m]; n];
  let mut memo2: Vec<Vec<u64>> = vec![vec![0; m]; n];
  memo1[0][0] = (ss[0] == ts[0]) as u64;
  memo2[0][0] = memo1[0][0];
  for i in 1..n {
    memo1[i][0] = (ss[i] == ts[0]) as u64;
    memo2[i][0] = memo2[i-1][0] + memo1[i][0];
  }
  for j in 1..m {
    memo1[0][j] = (ss[0] == ts[j]) as u64;
    memo2[0][j] = memo2[0][j-1] + memo1[0][j];
  }
  for i in 1..n {
    for j in 1..m {
      memo1[i][j] = if ss[i] == ts[j] { 1 + memo2[i-1][j-1] } else { 0 };
      memo2[i][j] = (memo2[i][j-1] + memo2[i-1][j] + (M - memo2[i-1][j-1]) + memo1[i][j]) % M;
    }
  }
  println!("{}", memo2[n-1][m-1] + 1);
}