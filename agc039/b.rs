use std::io::{self, BufRead};
use std::collections::{BTreeMap, BTreeSet};

fn dfs(es: &Vec<Vec<usize>>, path: &mut BTreeMap<usize, usize>, visited: &mut BTreeSet<usize>, n: usize) -> bool {
  if let Some(v) = path.get(&n) {
    let len = path.len() - v;
    return len % 2 == 0;
  }
  let size = path.len();
  path.insert(n, size);
  for c in &es[n] {
    if !visited.contains(c) && !dfs(es, path, visited, *c) {
      return false;
    }
  }
  path.remove(&n);
  visited.insert(n);
  true
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut g: Vec<Vec<i32>> = Vec::new();
  for _ in 0..n {
    let s = lines.next().unwrap().unwrap().chars().map(|c| if c == '1' { 1 } else { i32::max_value() }).collect();
    g.push(s);
  }
  for i in 0..n {
    g[i][i] = 0;
  }
  let mut path: BTreeMap<usize, usize> = BTreeMap::new();
  let mut es: Vec<Vec<usize>> = Vec::new();
  for i in 0..n {
    es.push(g[i].iter().enumerate().filter(|tup| *tup.1 == 1).map(|(i, _)| i).collect());
  }
  let mut visited: BTreeSet<usize> = BTreeSet::new();
  if !dfs(&es, &mut path, &mut visited, 0) {
    println!("-1");
    return;
  }
  for k in 0..n {
    for i in 0..n {
      for j in 0..n {
        if g[i][k] != i32::max_value() && g[k][j] != i32::max_value() && g[i][j] > g[i][k] + g[k][j] {
          g[i][j] = g[i][k] + g[k][j];
        }
      }
    }
  }
  let ans = g.iter().map(|l| l.iter().max().unwrap()).max().unwrap();
  println!("{}", ans + 1);
}