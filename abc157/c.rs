use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1]);
  let mut cs = vec![None; n];
  for _ in 0..m {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (s, c) = (xs[0] - 1, xs[1]);
    if s == 0 && c == 0 && n > 1 {
      println!("-1");
      return;
    }
    match cs[s] {
      None => cs[s] = Some(c),
      Some(d) => if c != d {
        println!("-1");
        return;
      },
    };
  }
  let mut ans = 0;
  for i in 0..n {
    match cs[i] {
      None    => ans = ans * 10 + (if i == 0 && n > 1 { 1 } else { 0 }),
      Some(c) => ans = ans * 10 + c,
    };
  }
  println!("{}", ans);
}