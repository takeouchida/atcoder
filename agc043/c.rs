use std::io::{self, BufRead};
use std::collections::{HashSet, BinaryHeap};
use std::cmp::Ordering;

const M: i64 = 998244353;
const ten18: i64 = 1_000_000_000_000_000_000 % M;

#[derive(PartialEq, Eq, Debug, Copy, Clone, Default, Hash)]
struct Vertex(usize, usize, usize, usize);

impl PartialOrd for Vertex {
    #[inline]
    fn partial_cmp(&self, other: &Vertex) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }

    #[inline]
    fn lt(&self, other: &Self) -> bool {
        self.0 < other.0
    }
    #[inline]
    fn le(&self, other: &Self) -> bool {
        self.0 <= other.0
    }
    #[inline]
    fn gt(&self, other: &Self) -> bool {
        self.0 > other.0
    }
    #[inline]
    fn ge(&self, other: &Self) -> bool {
        self.0 >= other.0
    }
}

impl Ord for Vertex {
    #[inline]
    fn cmp(&self, other: &Vertex) -> Ordering {
        self.0.cmp(&other.0)
    }
}

fn find_adjacent_vertices(edges: &Vec<Vec<HashSet<usize>>>, x: usize, y: usize, z: usize) -> Vec<(usize, usize, usize)> {
  let mut result = Vec::new();
  for i in &edges[0][x] {
    result.push((*i, y, z));
  }
  for i in &edges[1][y] {
    result.push((x, *i, z));
  }
  for i in &edges[2][z] {
    result.push((x, y, *i));
  }
  result
}

fn bfs(edges: &Vec<Vec<HashSet<usize>>>, passed: &mut HashSet<(usize, usize, usize)>, checked: &mut HashSet<(usize, usize, usize)>, forbidden: &mut HashSet<(usize, usize, usize)>, queue: &mut BinaryHeap<Vertex>) {
  while let Some(Vertex(p, x, y, z)) = queue.pop() {
    if passed.contains(&(x, y, z)) {
      continue;
    }
    passed.insert((x, y, z));
    if !forbidden.contains(&(x, y, z)) {
      checked.insert((x, y, z));
      for (x1, y1, z1) in find_adjacent_vertices(edges, x, y, z) {
        if !passed.contains(&(x1, y1, z1)) {
          forbidden.insert((x1, y1, z1));
        }
      }
    }
    for (x1, y1, z1) in find_adjacent_vertices(edges, x, y, z) {
      if !checked.contains(&(x1, y1, z1)) {
        queue.push(Vertex(x1+y1+z1, x1, y1, z1));
      }
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: usize = line.parse().unwrap();
  let mut edges: Vec<Vec<HashSet<usize>>> = vec![vec![HashSet::new(); n]; 3];
  for i in 0..3 {
    let line = lines.next().unwrap().unwrap();
    let m: usize = line.parse().unwrap();
    for _ in 0..m {
      let line = lines.next().unwrap().unwrap();
      let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).map(|v: usize| v - 1).collect();
      let j = vs[0];
      let k = vs[1];
      edges[i][j].insert(k);
      edges[i][k].insert(j);
    }
  }

  let mut checked: HashSet<(usize, usize, usize)> = HashSet::new();
  let mut forbidden: HashSet<(usize, usize, usize)> = HashSet::new();
  let mut passed: HashSet<(usize, usize, usize)> = HashSet::new();
  let mut queue: BinaryHeap<Vertex> = BinaryHeap::new();
  queue.push(Vertex(3*n-3, n-1, n-1, n-1));
  bfs(&edges, &mut passed, &mut checked, &mut forbidden, &mut queue);

  let mut memo: Vec<i64> = vec![0; 3*n+1];
  memo[0] = (ten18 * ten18 % M) * ten18 % M;
  for i in 1..(3*n+1) {
    memo[i] = memo[i-1] * ten18 % M;
  }
  let mut ans: i64 = 0;
  for (x, y, z) in checked {
    ans = (ans + memo[x + y + z]) % M;
  }

  println!("{}", ans);
}