use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let h = vs[0];
  let w = vs[1];
  let mut map: Vec<Vec<char>> = Vec::new();
  for _ in 0..h {
    let line = lines.next().unwrap().unwrap().chars().collect();
    map.push(line);
  }
  let mut memo: Vec<Vec<i32>> = vec![vec![0; w]; h];
  memo[0][0] = if map[0][0] == '#' { 1 } else { 0 };
  for n in 1..(h+w+1) {
    let i0 = if n < w { 0 } else { n - w + 1 };
    let i1 = if n < h { n } else { h - 1 };
    for i in i0..(i1+1) {
      let j = n - i;
      if i == 0 {
        memo[i][j] = memo[i][j-1] + (if map[i][j-1] == '.' && map[i][j] == '#' { 1 } else { 0 });
      } else if j == 0 {
        memo[i][j] = memo[i-1][j] + (if map[i-1][j] == '.' && map[i][j] == '#' { 1 } else { 0 });
      } else {
        let x = memo[i][j-1] + (if map[i][j-1] == '.' && map[i][j] == '#' { 1 } else { 0 });
        let y = memo[i-1][j] + (if map[i-1][j] == '.' && map[i][j] == '#' { 1 } else { 0 });
        memo[i][j] = min(x, y);
      }
    }
  }
  println!("{}", memo[h-1][w-1]);
}
