use std::io::{self, BufRead};
use std::mem::swap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let x: usize = line.parse().unwrap();
  let mut ns: Vec<i32> = lines.next().unwrap().unwrap().chars().map(|c| c as i32 - 48).collect();
  for _ in 1..x {
    let mut temp: Vec<i32> = vec![0; x];
    for i in 0..(x-1) {
      temp[i] = (ns[i] - ns[i+1]).abs();
    }
    swap(&mut ns, &mut temp);
  }
  println!("{}", ns[0]);
}