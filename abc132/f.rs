use std::io::{self, BufRead};
use std::mem::swap;

const M: usize = 1000000007;

fn create_div_floor(n: usize) -> Vec<usize> {
  let mut divfloor: Vec<usize> = Vec::new();
  let mut i: usize = 1;
  while i * i <= n {
    divfloor.push(i);
    i += 1;
  }
  let j = divfloor.len();
  for i in (0..j) {
    let k = n / divfloor[j-i-1];
    if *(divfloor.last().unwrap()) != k {
      divfloor.push(k);
    }
  }
  divfloor
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, k) = (xs[0], xs[1]);
  let df = create_div_floor(n);
  let mut counts = df.clone();
  for l in 0..(k-1) {
    let mut temp = vec![0; df.len()];
    temp[0] = counts[df.len()-1];
    for i in 1..df.len() {
      let j = df.len() - i - 1;
      let k = df[i] - df[i-1];
      temp[i] = (temp[i-1] + (k * counts[j] % M)) % M;
    }
    swap(&mut counts, &mut temp);
  }
  println!("{}", counts[df.len()-1]);
}