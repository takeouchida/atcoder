use std::io::{self, BufRead};
use std::mem::swap;
use std::collections::BTreeSet;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1]);
  let mut es: Vec<Vec<usize>> = vec![vec![]; n];
  for _ in 0..m {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (u, v) = (xs[0] - 1, xs[1] - 1);
    es[u].push(v);
  }
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (s, t) = (xs[0] - 1, xs[1] - 1);
  let mut dss: Vec<Vec<usize>> = vec![vec![usize::max_value(); 3]; n];
  dss[s][0] = 0;
  let mut nexts: BTreeSet<usize> = BTreeSet::new();
  nexts.insert(s);
  let mut step: usize = 1;
  while !nexts.is_empty() {
    let mut temp: BTreeSet<usize> =BTreeSet::new();
    let round = (step - 1) / 3 + 1;
    let phase = step % 3;
    for &u in &nexts {
      for &v in &es[u] {
        if round < dss[v][phase] {
          dss[v][phase] = round;
          temp.insert(v);
        }
      }
    }
    swap(&mut nexts, &mut temp);
    step += 1;
  }
  println!("{}", if dss[t][0] < usize::max_value() { dss[t][0] as i32 } else { -1 });
}