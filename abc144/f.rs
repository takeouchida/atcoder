use std::cmp::{Ordering, min};
use std::io::{self, BufRead};
use std::ops::{Add, Mul};

fn gcd(x: usize, y: usize) -> usize {
  let (mut a, mut b) = if x < y {
    (y, x)
  } else {
    (x, y)
  };
  while b != 0 {
    let r = a % b;
    a = b;
    b = r;
  }
  a
}

fn lcm_by_gcd(x: usize, y: usize, g: usize) -> usize {
  x * y / g
}

#[derive(Debug, Clone, Copy, Eq)]
struct Rational(usize, usize);

impl Add for Rational {
  type Output = Self;
  fn add(self, rhs: Self) -> Self::Output {
    let g = gcd(self.1, rhs.1);
    let l = lcm_by_gcd(self.1, rhs.1, g);
    Rational(self.0 * rhs.1 / g + rhs.0 * self.1 / g, l)
  }
}

impl Mul for Rational {
  type Output = Self;
  fn mul(self, rhs: Self) -> Self::Output {
    let n = self.0 * rhs.0;
    let d = self.1 * rhs.1;
    let g = gcd(n, d);
    Rational(n / g, d / g)
  }
}

impl Ord for Rational {
  fn cmp(&self, rhs: &Self) -> Ordering {
    let g = gcd(self.1, rhs.1);
    let l = self.0 * rhs.1 / g;
    let r = rhs.0 * self.1 / g;
    l.cmp(&r)
  }
}

impl PartialOrd for Rational {
  fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
    Some(self.cmp(rhs))
  }
}

impl PartialEq for Rational {
  fn eq(&self, rhs: &Self) -> bool {
    let g = gcd(self.1, rhs.1);
    let l = self.0 * rhs.1 / g;
    let r = rhs.0 * self.1 / g;
    l == r
  }
}

const ZERO: Rational = Rational(0, 1);
const ONE: Rational = Rational(1, 1);

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (vs[0], vs[1]);
  let mut es: Vec<Vec<usize>> = vec![Vec::new(); n];
  for _ in 0..m {
    let line = lines.next().unwrap().unwrap();
    let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
    let (s, t) = (vs[0] - 1, vs[1] - 1);
    es[s].push(t);
  }
  for i in 0..n {
    es[i].sort();
  }
  let mut memo0: Vec<Rational> = vec![ZERO; n];
  let mut memo1: Vec<Option<Rational>> = vec![None; n];
  for k in 1..n {
    let i = n - k - 1;
    let count = es[i].len();
    memo0[i] = ONE + Rational(1, count) * es[i].iter().map(|j| memo0[*j]).fold(ZERO, |acc, x| acc + x);
    if count > 1 {
      let mut temp = Vec::new();
      for j in 0..count {
        let x = (0..count).filter(|l| *l != j).map(|l| memo0[es[i][l]]).fold(ZERO, |acc, x| acc + x);
        let r1 = ONE + Rational(1, count - 1) * x;
        temp.push(r1);
        if let Some(y) = memo1[es[i][j]] {
          let r2 = ONE + Rational(1, count) * (x + y);
          temp.push(r2);
        }
      }
      memo1[i] = Some(*temp.iter().min().unwrap());
    }
  }
  let r = if let Some(r) = memo1[0] {
    min(memo0[0], r)
  } else {
    memo0[0]
  };
  let ans = r.0 as f64 / r.1 as f64;
  println!("{}", ans);
}