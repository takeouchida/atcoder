use std::io::{self, BufRead};
use std::cmp::min;

fn find_shortest(s: &[usize], n: usize, m: usize) -> Option<usize> {
  let mut i = 0;
  let mut count = 0;
  while i < n {
    let beg = i + 1;
    let end = min(n + 1, i + m + 1);
    let hurdle = s[beg..end].iter().enumerate().rev().find(|x| *x.1 == 0);
    match hurdle {
      None => return None,
      Some(h) => i += 1 + h.0,
    }
    count += 1;
  }
  Some(count)
}

fn find_smallest(s: &[usize], n: usize, m: usize, c: usize) -> Vec<usize> {
  let mut res: Vec<usize> = vec![];
  let mut i = 0;
  let mut next = 1;
  while res.len() < c {
    if m < next {
      println!("ng {} {:?}", i, res);
      match res.pop() {
        None => return vec![],
        Some(j) => {
          i -= j;
          next = j + 1;
        },
      }
    } else if s[i+next] == 0 && n <= i + next + (c - 1 - res.len()) * m {
      res.push(next);
      i += next;
      next = 1;
    } else {
      next += 1;
    }
  }
  res
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (vs[0], vs[1]);
  let s: Vec<usize> = lines.next().unwrap().unwrap().chars().map(|x| x as usize - 48).collect();
  let o = find_shortest(&s, n, m);
  match o {
    None => println!("-1"),
    Some(l) => {
      println!("{}", find_smallest(&s, n, m, l).iter().map(|x| x.to_string()).collect::<Vec<String>>().join(" "));
    },
  };
}