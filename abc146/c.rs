use std::io::{self, BufRead};
use std::cmp::{min, max};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let (a, b, x) = (vs[0], vs[1], vs[2]);
  let mut y: i64 = 10;
  let mut ans: i64 = 0;
  for i in 1..10 {
    let temp = (x - i * b) / a;
    if  y / 10 <= temp {
      ans = max(ans, min(y-1, temp));
    }
    y *= 10;
  }
  if 1000000000 <= (x - 10 * b) / a {
    ans = 1000000000;
  }
  println!("{}", ans);
}