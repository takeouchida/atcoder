use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut honest: Vec<Vec<usize>> = vec![Vec::new(); n];
  let mut dishonest: Vec<Vec<usize>> = vec![Vec::new(); n];
  for i in 0..n {
    let a: usize = lines.next().unwrap().unwrap().parse().unwrap();
    for _ in 0..a {
      let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let (x, y) = (xs[0] - 1, xs[1]);
      if y == 0 {
        dishonest[i].push(x);
      } else {
        honest[i].push(x);
      }
    }
  }
  let mut ans = 0;
  for i in 0..((2 as u32).pow(n as u32)) {
    let mut ok = true;
    for j in 0..n {
      if i & (1 << j) != 0 {
        let p1 = honest[j].iter().all(|h| i & (1 << *h) != 0);
        let p2 = dishonest[j].iter().all(|d| i & (1 << *d) == 0);
        if !p1 || !p2 {
          ok = false;
          break;
        }
      }
    }
    if ok {
      let temp = (0..n).filter(|k| i & (1 << *k) != 0).count();
      if ans < temp {
        ans = temp;
      }
    }
  }
  println!("{}", ans);
}