use std::io::{self, BufRead};

fn abssub(x: usize, y: usize) -> usize {
  if x < y { y - x } else { x - y }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (h, w) = (xs[0], xs[1]);
  let mut xss: Vec<Vec<usize>> = Vec::new();
  for _ in 0..h {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    xss.push(xs);
  }
  let mut yss: Vec<Vec<usize>> = Vec::new();
  for _ in 0..h {
    let ys: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    yss.push(ys);
  }
  let mut zss: Vec<Vec<usize>> = vec![vec![0; w]; h];
  for i in 0..h {
    for j in 0..w {
      zss[i][j] = abssub(xss[i][j], yss[i][j]);
    }
  }
  let max = zss.iter().map(|zs| zs.iter().max().unwrap()).max().unwrap();
  let m = (h + w + 1) * max + 1;
  let mut memo: Vec<Vec<Vec<bool>>> = vec![vec![vec![false; m]; w]; h];
  memo[0][0][zss[0][0]] = true;
  for i in 0..h {
    for j in 0..w {
      for k in 0..m {
        if j > 0 && memo[i][j-1][k] {
          memo[i][j][k+zss[i][j]] = true;
          memo[i][j][abssub(k, zss[i][j])] = true;
        }
        if i > 0 && memo[i-1][j][k] {
          memo[i][j][k+zss[i][j]] = true;
          memo[i][j][abssub(k, zss[i][j])] = true;
        }
      }
    }
  }
  let ans = memo[h-1][w-1].iter().position(|x| *x).unwrap();
  println!("{}", ans);
}