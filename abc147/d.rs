use std::io::{self, BufRead};

const D: u64 = 1000000007;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut ones: Vec<usize> = vec![0; 64];
  for i in 0..64 {
    ones[i] = xs.iter().filter(|x| **x & (1 << i) != 0).count();
  }
  let mut coeffs: Vec<u64> = vec![0; 64];
  coeffs[0] = 1;
  for i in 1..64 {
    coeffs[i] = coeffs[i-1] * 2 % D;
  }
  let mut ans: u64 = 0;
  for i in 0..64 {
    ans = (ans + ((ones[i] as u64 * (n as u64 - ones[i] as u64) % D) * coeffs[i] % D)) % D;
  }
  println!("{}", ans);
}