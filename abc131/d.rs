use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut xs: Vec<(usize, usize)> = Vec::new();
  for _ in 0..n {
    let ys: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    xs.push((ys[0], ys[1]));
  }
  xs.sort_by(|x, y| x.1.cmp(&y.1));
  let mut sum: usize = 0;
  for i in 0..n {
    sum += xs[i].0;
    if xs[i].1 < sum {
      println!("No");
      return;
    }
  }
  println!("Yes");
}