use std::io::{self, BufRead};

use std::collections::{BTreeMap, BTreeSet};

fn dfs(xys: &BTreeMap<u32, BTreeSet<u32>>, yxs: &BTreeMap<u32, BTreeSet<u32>>, xs: &Vec<u32>, ys: &Vec<u32>, visited: &mut Vec<Vec<bool>>, x: u32, y: u32) {
  let ix = xs.binary_search(&x).unwrap();
  let iy = ys.binary_search(&y).unwrap();
  visited[ix][iy] = true;
  if !yxs.contains_key(&y) {
    return;
  }
  for z in yxs.get(&y).unwrap() {
    if x == *z {
      continue;
    }
    if !xys.contains_key(&x) {
      continue;
    }
    for w in xys.get(&x).unwrap() {
      if y == *w {
        continue;
      }
      let iz = xs.binary_search(&z).unwrap();
      let iw = ys.binary_search(&w).unwrap();
      if visited[iz][iw] {
        continue;
      }
      dfs(xys, yxs, xs, ys, visited, *z, *w);
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut xys: BTreeMap<u32, BTreeSet<u32>> = BTreeMap::new();
  let mut yxs: BTreeMap<u32, BTreeSet<u32>> = BTreeMap::new();
  for _ in 0..n {
    let xs: Vec<u32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (x, y) = (xs[0], xs[1]);
    if !xys.contains_key(&x) {
      xys.insert(x, BTreeSet::new());
    }
    if let Some(ys) = xys.get_mut(&x) {
      ys.insert(y);
    }
    if !yxs.contains_key(&y) {
      yxs.insert(y, BTreeSet::new());
    }
    if let Some(xs) = yxs.get_mut(&y) {
      xs.insert(x);
    }
  }
  let kxs: Vec<u32> = xys.keys().cloned().collect();
  let kys: Vec<u32> = yxs.keys().cloned().collect();
  let mut visited: Vec<Vec<bool>> = vec![vec![false; kys.len()]; kxs.len()];
  for (x, ys) in &xys {
    for y in ys {
      dfs(&xys, &yxs, &kxs, &kys, &mut visited, *x, *y);
    }
  }
  let total: usize = visited.iter().map(|vs| vs.iter().filter(|v| **v).count()).sum();
  println!("{}", total - n);
}