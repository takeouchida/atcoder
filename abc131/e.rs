use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, k) = (xs[0], xs[1]);
  if n * (n - 1) / 2 - n + 1 < k {
    println!("-1");
    return;
  }
  let mut edges = n * (n - 1) / 2 - n + 1 - k;
  let count = n + edges - 1;
  println!("{}", count);
  for i in 2..(n+1) {
    println!("{} {}", 1, i);
  }
  for i in 2..n {
    for j in (i+1)..(n+1) {
      if edges == 0 {
        return;
      }
      println!("{} {}", i, j);
      edges -= 1;
    }
  }
}