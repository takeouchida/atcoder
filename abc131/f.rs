use std::io::{self, BufRead};
use std::collections::HashSet;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut xs: HashSet<usize> = HashSet::new();
  let mut ys: HashSet<usize> = HashSet::new();
  let mut zs: HashSet<(usize, usize)> = HashSet::new();
  for _ in 0..n {
    let vs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (x, y) = (vs[0], vs[1]);
    xs.insert(x);
    ys.insert(y);
    zs.insert((x, y));
  }
  println!("{:?}", xs);
  println!("{:?}", ys);
  println!("{:?}", zs);
}