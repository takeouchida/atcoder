use std::io::{self, BufRead};

fn gcd(x: u64, y: u64) -> u64 {
  let (mut a, mut b) = if x < y {
    (y, x)
  } else {
    (x, y)
  };
  while b != 0 {
    let r = a % b;
    a = b;
    b = r;
  }
  a
}

fn lcm_by_gcd(x: u64, y: u64, g: u64) -> u64 {
  x * y / g
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (a, b, c, d) = (xs[0], xs[1], xs[2], xs[3]);
  let g = gcd(c, d);
  let l = lcm_by_gcd(c, d, g);
  let total = b - a + 1;
  let div_c = b / c - (a - 1) / c;
  let div_d = b / d - (a - 1) / d;
  let div_cd = b / l - (a - 1) / l;
  let ans = total - (div_c + div_d - div_cd);
  println!("{}", ans);
}