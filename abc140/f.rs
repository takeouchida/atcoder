use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut ss: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  ss.sort_by(|a, b| b.cmp(a));
  println!("{:?}", ss);
  let mut base: usize = 1;
  for _ in 0..n {
    for i in 0..base {
      let j = i + base;
      println!("{} {} {} {}", i, ss[i], j, ss[i]);
      if ss[i] <= ss[j] {
        println!("No");
        return;
      }
    }
    base <<= 1;
  }
  println!("Yes");
}