use std::io::{self, BufRead};

fn dfs(pars: &mut Vec<Vec<Option<usize>>>, es: &Vec<Vec<(usize, usize)>>, visited: &mut Vec<bool>, n: usize) {
  visited[n] = true;
  for e in &es[n] {
    if visited[e.0] {
      continue;
    }
    pars[0][e.0] = Some(n);
    dfs(pars, es, visited, e.0);
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let zs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, q) = (zs[0], zs[1]);
  let mut es: Vec<Vec<(usize, usize)>> = vec![vec![]; n];
  let mut cs: Vec<usize> = vec![0; n - 1];
  let mut ds: Vec<usize> = vec![0; n - 1];
  for i in 0..(n-1) {
    let zs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (u, v) = (zs[0] - 1, zs[1] - 1);
    es[u].push((v, i));
    es[v].push((u, i));
    cs[i] = zs[2];
    ds[i] = zs[3];
  }
  let mut xs: Vec<usize> = vec![0; q];
  let mut ys: Vec<usize> = vec![0; q];
  let mut us: Vec<usize> = vec![0; q];
  let mut vs: Vec<usize> = vec![0; q];
  for i in 0..q {
    let zs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    xs[i] = zs[0];
    ys[i] = zs[1];
    us[i] = zs[2];
    vs[i] = zs[3];
  }
  let mut log_max_depth: usize = 0;
  let mut x: usize = 1;
  while x < n {
    log_max_depth += 1;
    x <<= 1;
  }
  let mut pars: Vec<Vec<Option<usize>>> = vec![vec![None; n]; log_max_depth];
  let mut visited = vec![false; n];
  dfs(&mut pars, &es, &mut visited, 0);
  for i in 1..log_max_depth {
    for j in 0..n {
      if let Some(k) = pars[i-1][j] {
        pars[i][j] = pars[i-1][k];
      }
    }
  }
  println!("{:?}", pars);
}