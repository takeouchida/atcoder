use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, k) = (xs[0], xs[1]);
  let ans = if k % 2 == 0 {
    let x = n / k;
    let y = (n + k / 2) / k;
    x * x * x + y * y * y
  } else {
    let x = n / k;
    x * x * x
  };
  println!("{}", ans);
}