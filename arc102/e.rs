use std::io::{self, BufRead};

const M: u64 = 998244353;

fn pow(n: u64, p: u64) -> u64 {
  let mut q = p;
  let mut r = n;
  let mut result = 1;
  while q > 0 {
    if q % 2 == 1 {
      result = result * r % M;
    }
    r = r * r % M;
    q >>= 1;
  }
  result
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (k, n) = (xs[0], xs[1]);

  let mut pow2: Vec<u64> = vec![1];
  for i in 0..n {
    pow2.push(pow2[i] * 2 % M);
  }
  let mut fib: Vec<u64> = vec![1];
  for i in 0..(n+k) {
    fib.push(fib[i] * (i + 1) as u64 % M);
  }
  let mut invfib: Vec<u64> = Vec::new();
  for i in &fib {
    invfib.push(pow(*i, M - 2));
  }

  for i in 2..(k+2) {
    let l = (i - 1) / 2;
    for j in 0..((i+1)/2) {
      if i % 2 == 0 {
        if i != k + 1 || j != 0 {
          println!("a {} {} {} 0", i, j, l);
          println!("a {} {} {} 1", i, j, l);
        }
      } else if i != k + 1 || j != 0 {
        println!("b {} {} {}", i, j, l);
      }
    }
  }
  for i in (k+2)..(2*k+1) {
    let j = 2 * k - i;
    println!("c {} {}", i, j);
  }
}