use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let l: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut n = 1;
  let mut temp = l;
  while temp > 1 {
    temp >>= 1;
    n += 1;
  }
  let mut beg: usize = usize::pow(2, n - 1);
  let mut ns: Vec<(usize, usize)> = Vec::new();
  for i in 0..(n-1) {
    let k = (n - i - 2) as usize;
    if l & (1 << k) != 0 {
      ns.push(((i + 2) as usize, beg));
      beg += usize::pow(2, k as u32);
    }
  }
  let m = (2 * (n - 1) as usize) + ns.len();
  println!("{} {}", n, m);
  for i in 0..(n-1) {
    println!("{} {} {}", i + 1, i + 2, 0);
    println!("{} {} {}", i + 1, i + 2, u32::pow(2, n - i - 2));
  }
  for (x, y) in ns {
    println!("{} {} {}", 1, x, y);
  }
}