use std::io::{self, BufRead};

fn to_index(c: char) -> usize {
  match c {
    'R' => 0,
    'G' => 1,
    'B' => 2,
    _ => panic!("to_index: Logic error: {}", c),
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let s: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
  let mut ixs: Vec<Vec<u64>> = vec![Vec::new(); 3];
  for i in 0..s.len() {
    let j = to_index(s[i]);
    ixs[j].push(i as u64);
  }
  let mut balls: Vec<Vec<u64>> = vec![Vec::new(); 3];
  for i in 0..n {
    let mut temp = vec![ixs[0][i], ixs[1][i], ixs[2][i]];
    temp.sort();
    for j in 0..3 {
      balls[j].push(temp[j]);
    }
  }
  println!("{:?}", balls);
}