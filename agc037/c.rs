use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut ys: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut zs: BTreeSet<usize> = BTreeSet::new();
  for i in 0..n {
    let j = (i + 1) % n;
    let k = (i + 2) % n;
    if ys[i] + ys[k] < ys[j] && xs[j] <= ys[j] - ys[i] - ys[k] {
      zs.insert(i);
    }
  }
  let mut count: usize = 0;
  while !zs.is_empty() {
    let &i = zs.iter().next().unwrap();
    let j = (i + 1) % n;
    let k = (i + 2) % n;
    let q = (ys[j] - xs[j]) / (ys[i] + ys[k]);
    ys[j] -= (ys[i] + ys[k]) * q;
    zs.remove(&i);
    let l = (i + n - 1) % n;
    let m = (i + 3) % n;
    if ys[i] + ys[k] < ys[j] && xs[j] <= ys[j] - ys[i] - ys[k] {
      zs.insert(i);
    }
    if ys[l] + ys[j] < ys[i] && xs[i] <= ys[i] - ys[l] - ys[j] {
      zs.insert(l);
    }
    if ys[j] + ys[m] < ys[k] && xs[k] <= ys[k] - ys[j] - ys[m] {
      zs.insert(j);
    }
    count += q;
  }
  if xs == ys {
    println!("{}", count);
  } else {
    println!("-1");
  }
}