use std::io::{self, BufRead};

fn parin(s: &[u8]) -> bool {
  let n = s.len();
  for i in 0..(n/2) {
    if s[i as usize] != s[(n-1-i) as usize] {
      return false;
    }
  }
  true
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let s = lines.next().unwrap().unwrap();
  let bytes = s.as_bytes();
  let n = bytes.len();
  let ans = if parin(bytes) && parin(&bytes[0..((n-1)/2)]) && parin(&bytes[((n+1)/2)..n]) { "Yes" } else { "No" };
  println!("{}", ans);
}