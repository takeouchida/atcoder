use std::io::{self, BufRead};

fn dp(memo: &mut Vec<Vec<Vec<i32>>>, vs: &Vec<i32>, i: usize, j: usize, s: i32) -> i32{
  let m = memo[i][j][s as usize];
  if m >= 0 {
    m
  } else if i == j {
    if s == vs[j] || s == 0 {
      1
    } else {
      0
    }
  } else {
    let v = vs[j];
    let result = if s < v {
      dp(memo, vs, i, j - 1, s)
    } else {
      dp(memo, vs, i, j - 1, s - v) + dp(memo, vs, i, j - 1, s)
    };
    memo[i][j][s as usize] = result;
    result
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let s = vs[1] as i32;
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i32> = line.split(' ').map(|x| x.parse().unwrap()).collect();

  let mut memo = vec![vec![vec![-1; (s+1) as usize]; n]; n];
  let mut sum = 0;

  for i in 0..n {
    for j in i..n {
      let result = dp(&mut memo, &vs, i, j, s);
      sum += result;
    }
  }

  println!("{}", sum);
}