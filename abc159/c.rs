use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let x: f64 = line.parse().unwrap();
  let l = x / 3.0;
  let v = l * l * l;
  println!("{}", v);
}