use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i32> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let m = vs[1];
  let ans_n = if n == 0 { 0 } else { n * (n - 1) / 2 };
  let ans_m = if m == 0 { 0 } else { m * (m - 1) / 2 };
  println!("{}", ans_n + ans_m);
}