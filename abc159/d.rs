use std::io::{self, BufRead};
use std::collections::HashMap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let _ = lines.next().unwrap().unwrap();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i32> = line.split(' ').map(|x| x.parse().unwrap()).collect();

  let mut groups: HashMap<i32, usize> = HashMap::new();
  for v in &vs {
    if groups.contains_key(v) {
      let v1 = groups[v] + 1;
      groups.insert(*v, v1);
    } else {
      groups.insert(*v, 1);
    }
  }

  let counts: HashMap<i32, usize> = groups.iter().map(|(k, n)| (*k, n * (n - 1) / 2)).collect();
  let counts1: HashMap<i32, usize> = groups.iter().map(|(k, n)| if *n <= 1 { (*k, 0) } else { (*k, (n - 1) * (n - 2) / 2) }).collect();
  let total: usize = counts.values().sum();

  for v in &vs {
    println!("{}", total - counts[v] + counts1[v]);
  }
}