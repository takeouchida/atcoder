use std::io::{self, BufRead};

fn gcd(x: usize, y: usize) -> usize {
  let (mut a, mut b) = if x < y {
    (y, x)
  } else {
    (x, y)
  };
  while b != 0 {
    let r = a % b;
    a = b;
    b = r;
  }
  a
}

fn lcm_by_gcd(x: usize, y: usize, g: usize) -> usize {
  x * y / g
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let g = gcd(xs[0], xs[1]);
  let l = lcm_by_gcd(xs[0], xs[1], g);
  println!("{}", l);
}