use std::io::{self, BufRead};

fn dfs(i: usize, u: usize, d: usize, es: &Vec<Vec<usize>>, du: &mut usize, visited: &mut Vec<bool>) {
  visited[i] = true;
  if i == u {
    *du = d;
  }
  for j in &es[i] {
    if !visited[*j] {
      dfs(*j, u, d+1, es, du, visited);
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, u, v) = (xs[0], xs[1] - 1, xs[2] - 1);
  let mut es: Vec<Vec<usize>> = vec![Vec::new(); n];
  for _ in 0..(n-1) {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (j, k) = (xs[0] - 1, xs[1] - 1);
    es[j].push(k);
    es[k].push(j);
  }
  let mut du = 0;
  let mut visited: Vec<bool> = vec![false; n];
  visited[v] = true;
  dfs(v, u, 0, &es, &mut du, &mut visited);
  let ans = (du - 1) / 2 * 2;
  println!("{}", ans);
}