use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: u64 = lines.next().unwrap().unwrap().parse().unwrap();
  if n % 2 != 0 {
    println!("0");
    return;
  }
  let mut count: u64 = 0;
  let mut i: u64 = 10;
  while i <= n {
    count += n / i;
    i *= 5;
  }
  println!("{}", count);
}