use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let l1 = lines.next().unwrap();
  let v1: Vec<i32> = l1.unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let l2 = lines.next().unwrap();
  let v2: Vec<i32> = l2.unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let total = v2.iter().fold(0, |x, y| x + y);
  let ans = if v2.iter().filter(|x| **x * 4 * v1[1] >= total).count() >= (v1[1] as usize) { "Yes" } else { "No" };
  println!("{}", ans);
}