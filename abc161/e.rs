use std::collections::BTreeSet;
use std::io::{self, BufRead};

fn find_count(cal: &[u8], d: usize, k: usize, c: usize, memo: &mut Vec<Vec<Option<Box<BTreeSet<usize>>>>>) -> Option<Box<BTreeSet<usize>>> {
  if k == 0 {
    return Some(Box::new(BTreeSet::new()));
  }
  if cal.len() <= d {
    return None;
  }
  if let &Some(cnt) = &memo[d][k-1] {
    return Some(cnt.clone());
  }
  const O: u8 = 'o' as u8;
  const X: u8 = 'x' as u8;
  let cnt = if cal[d] == O {
    let o1 = find_count(cal, d + c + 1, k - 1, c, memo);
    let o2 = find_count(cal, d + 1, k, c, memo);
    match (o1.clone(), o2.clone()) {
      (Some(s1), Some(s2)) => {
        Some(Box::new(s1.intersection(&s2).map(|x| x.clone()).collect::<BTreeSet<usize>>()))
      },
      (Some(s1), None    ) => {
        let mut s1_ : Box<BTreeSet<usize>> = Box::new((*s1).clone());
        s1_.insert(d);
        Some(s1_)
      },
      (None,     Some(s2)) => {
        let mut s2_ = Box::new((*s2).clone());
        s2_.insert(d);
        Some(s2_)
      }
      (None,     None    ) => None,
    }
  } else if cal[d] == X {
    find_count(cal, d + 1, k, c, memo)
  } else {
    panic!("unsupported char")
  };
  memo[d][k-1] = cnt.clone();
  cnt
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let l1 = lines.next().unwrap().unwrap();
  let v1: Vec<i32> = l1.split(' ').map(|x| x.parse().unwrap()).collect();
  let cal: String = lines.next().unwrap().unwrap();
  let n = v1[0] as usize;
  let k = v1[1] as usize;
  let c = v1[2] as usize;

  let mut memo: Vec<Vec<Option<Box<BTreeSet<usize>>>>> = vec![vec![None; k]; n];

  let cnt = find_count((&cal).as_bytes(), 0, k, c, &mut memo);

  if let Some(s) = cnt {
    for i in *s {
      println!("{}", i + 1);
    }
  }
}