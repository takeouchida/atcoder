use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let ds: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let s: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
  let n = ds[0];
  let k = ds[1];
  let c = ds[2];

  let mut l = vec![-1 as i32; k as usize];
  let mut d: i32 = 0;
  let mut w: i32 = 0;
  while d < n && w < k {
    if s[d as usize] == 'o' {
      l[w as usize] = d;
      w += 1;
      d += c + 1;
    } else {
      d += 1;
    }
  }

  let mut r = vec![-1 as i32; k as usize];
  let mut d: i32 = n - 1;
  let mut w: i32 = k - 1;
  while d >= 0 && w >= 0 {
    if s[d as usize] == 'o' {
      r[w as usize] = d;
      w -= 1;
      d -= c + 1;
    } else {
      d -= 1;
    }
  }

  for i in 0..k {
    if l[i as usize] == r[i as usize] {
      println!("{}", l[i as usize] + 1);
    }
  }
}