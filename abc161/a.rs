use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  for line in stdin.lock().lines() {
    let mut is: Vec<i32> = line.unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let tmp = is[0];
    is[0] = is[1];
    is[1] = tmp;
    let tmp = is[0];
    is[0] = is[2];
    is[2] = tmp;
    println!("{} {} {}", is[0], is[1], is[2]);
  }
}