use std::io::{self, BufRead};

pub fn find_divisors(n: i64) -> Vec<i64> {
  let mut res = Vec::new();
  let mut i = 1;
  while i * i <= n {
    if n % i == 0 {
      res.push(i);
      if i != n / i {
        res.push(n / i);
      }
    }
    i += 1
  }
  res
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: i64 = lines.next().unwrap().unwrap().parse().unwrap();

  let num_divs_n = find_divisors(n-1).len() - 1;
  let divs = find_divisors(n);
  let mut num_divs_n_1 = 0;
  for k in divs {
    if n % k != 0 {
      continue
    }
    let mut m = n;
    while k != 1 && m != 1 && m % k == 0 {
      m /= k;
    }
    if m % k == 1 {
      num_divs_n_1 += 1;
    }
  }

  println!("{}", num_divs_n_1 + num_divs_n);
}