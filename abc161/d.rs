use std::io::{self, BufRead};
use std::cmp::{min, max};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap();
  let vec: Vec<i32> = line.unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let input = vec[0];

  let mut counts = vec![vec![1; 10]; 1];
  // counts[0][0] = 0;
  for i in 0..10 {
    let mut vec1 = vec![0; 10];
    for j in 0..10 {
      let beg = if j == 0 { 0 } else { j - 1 };
      let end = min(10, j + 2);
      vec1[j] = counts[i][beg..end].iter().fold(0, |x, y| x + y);
    }
    counts.push(vec1);
  }
  let mut counts2 = vec![0; 11];
  for i in 1..11 {
    counts2[i] = counts[i-1][1..].iter().fold(0, |x, y| x + y) + counts2[i-1];
  }

  let mut output = String::new();
  let mut res = input;
  let mut prev_digit: i32 = -1;
  for i in (0..11).rev() {
    if prev_digit == -1 && res < counts2[i] {
      continue;
    }
    if prev_digit == -1 {
      res -= counts2[i];
    }
    let beg = (if prev_digit == -1 { 1 } else { max(0, prev_digit - 1) }) as usize;
    let end = (if prev_digit == -1 { 10 } else { min(10, prev_digit + 2) }) as usize;
    for j in beg..end {
      if counts[i][j] >= res {
        output.push((j as u8 + 48) as char);
        prev_digit = j as i32;
        break;
      }
      res -= counts[i][j];
    }
  }

  println!("{}", output);
}