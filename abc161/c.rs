use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap();
  let vec: Vec<i64> = line.unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let res = vec[0] % vec[1];
  let ans = std::cmp::min(res, vec[1] - res);
  println!("{}", ans);
}