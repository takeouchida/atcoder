use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: usize = line.parse().unwrap();
  let line = lines.next().unwrap().unwrap();
  let ps: Vec<i32> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let mut min = ps[0];
  let count = 1;
  for i in 1..n {
    if ps[i] <= min {
      min = ps[i];
      count += 1;
    }
  }
  println!("{}", count);
}