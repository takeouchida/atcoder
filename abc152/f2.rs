use std::io::{self, BufRead};
use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::mem::swap;

fn lca(parents: &Vec<(usize, usize)>, x: usize, y: usize) -> usize {
  let mut u = x;
  let mut v = y;
  let depth_x = parents[x].1;
  let depth_y = parents[y].1;
  if depth_x < depth_y {
    for _ in 0..(depth_y - depth_x) {
      v = parents[v].0;
    }
  } else {
    for _ in 0..(depth_x - depth_y) {
      u = parents[u].0;
    }
  }
  while u != v {
    u = parents[u].0;
    v = parents[v].0;
  }
  u
}

fn insert_or_update<K, V>(tree: &mut HashMap<K, HashSet<V>>, k: &K, v: &V) where K: Eq + Hash + Copy, V: Eq + Hash + Copy {
  let contains = tree.contains_key(k);
  if contains {
    tree.get_mut(k).unwrap().insert(*v);
  } else {
    let mut ws = HashSet::new();
    ws.insert(*v);
    tree.insert(*k, ws);
  }
}

fn create_parents(tree: &HashMap<usize, HashSet<usize>>, n: usize) -> Vec<(usize, usize)> {
  let mut parents: Vec<(usize, usize)> = vec![(0, usize::max_value()); n];
  let mut buf = Vec::new();
  buf.push(0);
  parents[0].1 = 0;
  let mut depth = 1;
  while !buf.is_empty() {
    let mut next = Vec::new();
    for i in &buf {
      if let Some(js) = tree.get(&i) {
        for j in js {
          if parents[*j].1 == usize::max_value() {
            parents[*j] = (*i, depth);
            next.push(*j);
          }
        }
      }
    }
    swap(&mut buf, &mut next);
    depth += 1;
  }
  parents
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut tree: HashMap<usize, HashSet<usize>> = HashMap::new();

  for _ in 0..(n-1) {
    let vs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).map(|x: usize| x - 1).collect();
    insert_or_update(&mut tree, &vs[0], &vs[1]);
    insert_or_update(&mut tree, &vs[1], &vs[0]);
  }

  let m: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut constr: Vec<(usize, usize)> = Vec::new();
  for _ in 0..m {
    let vs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).map(|x: usize| x - 1).collect();
    constr.push((vs[0], vs[1]));
  }

  let parents = create_parents(&tree, n);

  let lca_memo: Vec<_> = (0..m).map(|i| lca(&parents, constr[i].0, constr[i].1)).collect();
  let num_paths = 1 << m;
  let mut total: i32 = 0;
  let num_all = 1 << (n - 1);
  for paths in 1..num_paths {
    let cs: Vec<usize> = (0..m).filter(|i| paths & (1 << i) != 0).collect();
    let sign = if cs.len() % 2 == 0 { -1 } else { 1 };
    let mut edges = HashSet::new();
    for c in &cs {
      let (mut x, mut y) = constr[*c];
      let l = lca_memo[*c];
      while x != l {
        edges.insert(x);
        x = parents[x].0;
      }
      while y != l {
        edges.insert(y);
        y = parents[y].0;
      }
    }
    let size = n - 1 - edges.len();
    total += sign * (1 << size) as i32;
  }
  println!("{}", num_all - total);
}