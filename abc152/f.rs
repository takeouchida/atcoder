use std::io::{self, BufRead};
use std::collections::{HashMap, HashSet};
use std::mem::swap;

fn lca(parents: &Vec<(i32, i32)>, x: i32, y: i32) -> i32 {
  let mut u = x;
  let mut v = y;
  let depth_x = parents[x as usize].1;
  let depth_y = parents[y as usize].1;
  if depth_x < depth_y {
    for _ in 0..(depth_y - depth_x) {
      v = parents[v as usize].0;
    }
  } else {
    for _ in 0..(depth_x - depth_y) {
      u = parents[u as usize].0;
    }
  }
  while u != v {
    u = parents[u as usize].0;
    v = parents[v as usize].0;
  }
  u
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: usize = line.parse().unwrap();
  let mut tree: HashMap<i32, HashSet<i32>> = HashMap::new();

  for _ in 0..(n-1) {
    let line = lines.next().unwrap().unwrap();
    let vs: Vec<i32> = line.split(' ').map(|x| x.parse().unwrap()).map(|x: i32| x - 1).collect();
    let contains = tree.contains_key(&vs[0]);
    if contains {
      tree.get_mut(&vs[0]).unwrap().insert(vs[1]);
    } else {
      let mut ws = HashSet::new();
      ws.insert(vs[1]);
      tree.insert(vs[0], ws);
    }
    let contains = tree.contains_key(&vs[1]);
    if contains {
      tree.get_mut(&vs[1]).unwrap().insert(vs[0]);
    } else {
      let mut ws = HashSet::new();
      ws.insert(vs[0]);
      tree.insert(vs[1], ws);
    }
  }

  let line = lines.next().unwrap().unwrap();
  let m: i32 = line.parse().unwrap();
  let mut constr: Vec<(i32, i32)> = Vec::new();
  for _ in 0..m {
    let line = lines.next().unwrap().unwrap();
    let vs: Vec<i32> = line.split(' ').map(|x| x.parse().unwrap()).map(|x: i32| x - 1).collect();
    constr.push((vs[0], vs[1]));
  }

  let mut parents: Vec<(i32, i32)> = vec![(0, -1); n];
  for i in 0..n {
    parents[i].0 = i as i32;
  }
  let mut buf = Vec::new();
  buf.push(0);
  parents[0].1 = 0;
  let mut depth = 1;
  while !buf.is_empty() {
    let mut next = Vec::new();
    for i in &buf {
      if let Some(js) = tree.get(&i) {
        for j in js {
          if parents[*j as usize].1 < 0 {
            parents[*j as usize] = (*i, depth);
            next.push(*j);
          }
        }
      }
    }
    swap(&mut buf, &mut next);
    depth += 1;
  }

  let mut lca_memo = vec![0; m as usize];
  for i in 0..m {
    lca_memo[i as usize] = lca(&parents, constr[i as usize].0, constr[i as usize].1);
  }
  let num_paths = 1 << m;
  let mut total = 0;
  let num_all = 1 << (n - 1);
  for paths in 1..num_paths {
    let cs: Vec<i32> = (0..m).filter(|i| paths & (1 << i) != 0).collect();
    let sign = if cs.len() % 2 == 0 { -1 } else { 1 };
    let mut edges = HashSet::new();
    for c in &cs {
      let mut x = constr[*c as usize].0;
      let mut y = constr[*c as usize].1;
      let l = lca_memo[*c as usize];
      while x != l {
        edges.insert((x, parents[x as usize].0));
        x = parents[x as usize].0;
      }
      while y != l {
        edges.insert((y, parents[y as usize].0));
        y = parents[y as usize].0;
      }
    }
    let size = n - 1 - edges.len();
    total += sign * (1 << size);
  }
  println!("{}", num_all - total);
}