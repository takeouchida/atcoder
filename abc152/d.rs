use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: i32 = line.parse().unwrap();
  let mut memo = vec![vec![0; 9]; 9];
  for i in 1..(n+1) {
    let si = format!("{}", i);
    let z: usize = (0..(si.len()-1)).map(|_| 10).product();
    let most: usize = i as usize / z;
    let least: usize = i as usize % 10;
    if least == 0 {
      continue;
    }
    memo[most-1][least-1] += 1;
  }
  let mut count = 0;
  for i in 0..9 {
    for j in 0..9 {
      count += memo[i][j] * memo[j][i];
    }
  }
  println!("{}", count);
}