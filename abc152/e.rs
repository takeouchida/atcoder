use std::io::{self, BufRead};
use std::collections::HashMap;

const M: i64 = 1000000007;

pub fn find_prime_factors(n: i64) -> HashMap<i64, i64> {
  let mut res = HashMap::new();
  let mut i = 2;
  let mut m = n;
  while i * i <= m {
    while m % i == 0 {
      let val = 1 + res.get(&i).unwrap_or(&0);
      res.insert(i, val);
      m /= i;
    }
    i += 1
  }
  if m != 1 {
    let val = 1 + res.get(&m).unwrap_or(&0);
    res.insert(m, val);
  }
  res
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let _ = lines.next();
  let line = lines.next().unwrap().unwrap();
  let mut max_x: HashMap<i64, i64> = HashMap::new();
  let xs: Vec<_> = line.split(' ').map(|x| x.parse().unwrap()).map(|x| {
    let x = find_prime_factors(x);
    for (p, n) in x.iter() {
      if max_x.contains_key(p) {
        let m = max_x.get(p).map(|m| *m).unwrap();
        if m < *n {
          max_x.insert(*p, *n);
        }
      } else {
        max_x.insert(*p, *n);
      }
    }
    x
  }).collect();

  let mut memo: HashMap<i64, Vec<i64>> = HashMap::new();
  for (p, n) in max_x.iter() {
    let mut x = 1;
    let mut v = vec![1];
    for _ in 1..(n+1) {
      x = x * p % M;
      v.push(x);
    }
    memo.insert(*p, v);
  }

  let ans = xs.iter().map(|x| {
    let mut z = 1;
    for (p, n) in max_x.iter() {
      let n0 = x.get(p).map(|p| *p).unwrap_or(0);
      let l = *n - n0;
      z = z * memo.get(p).unwrap()[l as usize] % M;
    }
    z
  }).fold(0, |acc, z| (acc + z) % M);
  println!("{}", ans);
}