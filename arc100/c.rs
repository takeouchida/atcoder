use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let _: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let a: i32 = xs.iter().enumerate().map(|(i, x)| x - i as i32).min().unwrap();
  let mut ds: Vec<i32> = xs.iter().enumerate().map(|(i, x)| x - i as i32 - a).collect();
  ds.sort();
  let mut ans: i32 = ds.iter().sum();
  let mut sum: i32 = ans;
  let mut b: i32 = ds[0];
  let mut i: usize = 0;
  while i < ds.len() {
    if b != ds[i] {
      sum = sum - (ds.len() - i) as i32 * (ds[i] - b) + i as i32 * (ds[i] - b);
      ans = min(ans, sum);
      b = ds[i];
    }
    i += 1;
  }
  println!("{}", ans);
}