use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut xss: Vec<Vec<usize>> = Vec::new();
  for _ in 0..n {
    let zs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse::<usize>().unwrap() - 1).collect();
    xss.push(zs);
  }
  let mut ixs: Vec<usize> = vec![0; n];
  let mut ds: Vec<usize> = vec![0; n];
  let mut i: usize = 0;
  let mut updated = false;
  loop {
    if ixs[i] < xss[i].len() {
      let j = xss[i][ixs[i]];
      if ixs[j] < xss[j].len() {
        let k = xss[j][ixs[j]];
        if i == k {
          updated = true;
          ixs[i] += 1;
          ixs[j] += 1;
          let d = max(ds[i], ds[j]) + 1;
          ds[i] = d;
          ds[j] = d;
        }
      }
    }
    i += 1;
    if n <= i {
      if !updated {
        break;
      }
      i = 0;
      updated = false;
    }
  }
  let ok = (0..n).all(|i| xss[i].len() == ixs[i]);
  if ok {
    println!("{}", ds.iter().max().unwrap());
  } else {
    println!("-1");
  }
}