use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let hs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut max_h: usize = 0;
  let mut temp: usize = 0;
  for i in 1..n {
    if hs[i-1] >= hs[i] {
      temp += 1;
    } else {
      max_h = max(max_h, temp);
      temp = 0;
    }
  }
  max_h = max(max_h, temp);
  println!("{}", max_h);
}