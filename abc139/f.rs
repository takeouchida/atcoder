use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut xs: Vec<i64> = Vec::new();
  let mut ys: Vec<i64> = Vec::new();
  for _ in 0..n {
    let zs: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    xs.push(zs[0]);
    ys.push(zs[1]);
  }
  let mut max_xs: Vec<Vec<i64>> = vec![vec![0; n]; 4];
  let mut max_ys: Vec<Vec<i64>> = vec![vec![0; n]; 4];
  let mut max_ds: Vec<Vec<i64>> = vec![vec![0; n]; 4];
  let d0 = xs[0] * xs[0] + ys[0] * ys[0];
  if xs[0] >= 0 && ys[0] >= 0 {
    max_xs[0][0] = xs[0]; max_ys[0][0] = ys[0]; max_ds[0][0] = d0;
  }
  if xs[0] <= 0 && ys[0] >= 0 {
    max_xs[1][0] = xs[0]; max_ys[1][0] = ys[0]; max_ds[1][0] = d0;
  }
  if xs[0] <= 0 && ys[0] <= 0 {
    max_xs[2][0] = xs[0]; max_ys[2][0] = ys[0]; max_ds[2][0] = d0;
  }
  if xs[0] >= 0 && ys[0] <= 0 {
    max_xs[3][0] = xs[0]; max_ys[3][0] = ys[0]; max_ds[3][0] = d0;
  }
  for j in 1..n {
    for i in 0..4 {
      let x = max_xs[i][j-1] + xs[j];
      let y = max_ys[i][j-1] + ys[j];
      let d = x * x + y * y;
      if x >= 0 && y >= 0 && max_ds[0][j-1] < d && max_ds[0][j] < d {
        max_xs[0][j] = x; max_ys[0][j] = y; max_ds[0][j] = d;
      }
      if x <= 0 && y >= 0 && max_ds[1][j-1] < d && max_ds[1][j] < d {
        max_xs[1][j] = x; max_ys[1][j] = y; max_ds[1][j] = d;
      }
      if x <= 0 && y <= 0 && max_ds[2][j-1] < d && max_ds[2][j] < d {
        max_xs[2][j] = x; max_ys[2][j] = y; max_ds[2][j] = d;
      }
      if x >= 0 && y <= 0 && max_ds[3][j-1] < d && max_ds[3][j] < d {
        max_xs[3][j] = x; max_ys[3][j] = y; max_ds[3][j] = d;
      }
    }
    for i in 0..4 {
      if max_ds[i][j] == 0 {
        max_xs[i][j] = max_xs[i][j-1]; max_ys[i][j] = max_ys[i][j-1]; max_ds[i][j] = max_ds[i][j-1];
      }
    }
  }
  let m = (0..4).map(|i| max_ds[i][n-1]).max().unwrap();
  let ans = (m as f64).sqrt();
  println!("{}", ans);
}