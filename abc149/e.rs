use std::cmp::Ordering;
use std::io::{self, BufRead};
use std::collections::BinaryHeap;

#[derive(Debug)]
struct Pair(usize, usize, usize);

impl Ord for Pair {
  fn cmp(&self, other: &Self) -> Ordering {
    self.0.cmp(&other.0)
  }
}

impl PartialOrd for Pair {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.0.cmp(&other.0))
  }
}

impl PartialEq for Pair {
  fn eq(&self, other: &Self) -> bool {
    self.0 == other.0
  }
}

impl Eq for Pair {}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let mut m = vs[1];
  let line = lines.next().unwrap().unwrap();
  let mut xs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  xs.sort_by(|a, b| b.cmp(&a));
  let mut buf: BinaryHeap<Pair> = BinaryHeap::new();
  let mut score = 0;
  buf.push(Pair(xs[0]*2, 0, 0));
  while let Some(Pair(p, x, y)) = buf.pop() {
    if m == 0 {
      break;
    }
    score += p;
    m -= 1;
    if x < n-1 && y == 0 {
      buf.push(Pair(xs[x+1] + xs[0], x+1, 0));
    }
    if y < n-1 {
      buf.push(Pair(xs[x] + xs[y+1], x, y+1));
    }
  }
  println!("{}", score);
}