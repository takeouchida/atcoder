use std::io::{self, BufRead};
use std::collections::HashMap;
use std::cmp::max;
use std::mem::swap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let k = vs[1];
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let r = vs[0];
  let s = vs[1];
  let p = vs[2];
  let t: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
  let points: Vec<Vec<usize>> = vec![vec![0, 0, p], vec![r, 0, 0], vec![0, s, 0]];
  let char_to_usize: HashMap<char, usize> = [('r', 0), ('s', 1), ('p', 2)].iter().cloned().collect();
  let mut total: usize = 0;
  for i in 0..k {
    let len = n / k + (if i < n % k { 1 } else { 0 });
    let mut temp: Vec<usize> = vec![0; 3];
    let o = char_to_usize[&t[i]];
    for s in 0..3 {
      temp[s] = points[o][s];
    }
    for j in 1..len {
      let mut next: Vec<usize> = vec![0; 3];
      let o = char_to_usize[&t[j*k+i]];
      next[0] = max(temp[1] + points[o][0], temp[2] + points[o][0]);
      next[1] = max(temp[2] + points[o][1], temp[0] + points[o][1]);
      next[2] = max(temp[0] + points[o][2], temp[1] + points[o][2]);
      swap(&mut temp, &mut next);
    }
    total += *temp.iter().max().unwrap();
  }
  println!("{}", total);
}