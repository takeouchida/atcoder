use std::io::{self, BufRead};

pub fn is_prime(n: i64) -> bool {
  let mut i = 2;
  while i * i <= n {
    if n % i == 0 {
      return false
    }
    i += 1
  }
  n != 1
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let mut x: i64 = line.parse().unwrap();
  while !is_prime(x) {
    x += 1;
  }
  println!("{}", x);
}