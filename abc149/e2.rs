use std::io::{self, BufRead};

fn upper_bound(vec: &[i64], val: i64) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] < val {
      hi = p;
    } else {
      lo = p + 1;
    }
  }
  return lo;
}

pub fn lower_bound<T: Ord>(vec: &[T], val: T) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] > val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

fn find_count(xs: &[i64], sums: &[i64], val: i64) -> (usize, i64) {
  let mut count = 0;
  let mut sum = 0;
  for i in 0..xs.len() {
    let u = upper_bound(xs, val - xs[i]);
    count += u;
    let s = if u == 0 { 0 } else { sums[u-1] + xs[i] * u as i64 };
    sum += s;
    println!("{} {} {}", i, u, s);
  }
  (count, sum)
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let m = vs[1];
  let line = lines.next().unwrap().unwrap();
  let mut xs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  xs.sort_by(|a, b| b.cmp(&a));
  let mut sums: Vec<i64> = vec![0; n];
  sums[0] = xs[0];
  for i in 1..xs.len() {
    sums[i] = sums[i-1] + xs[i];
  }
  let mut lo = 0;
  let mut hi = xs[0] * 2;
  let mut sum = 0;
  while hi - lo > 0 {
    let p = (hi - lo) / 2;
    let (i, val) = find_count(&xs, &sums, p);
    sum = val;
    if i >= m {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  println!("{}", sum);
}