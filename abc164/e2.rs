use std::cmp::{Ordering, max, min};
use std::io::{self, BufRead};
use std::collections::{BinaryHeap, HashMap, HashSet};

struct Vertex(usize, usize, usize);

impl Ord for Vertex {
  fn cmp(&self, other: &Self) -> Ordering {
    other.0.cmp(&self.0)
  }
}

impl PartialOrd for Vertex {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(other.0.cmp(&self.0))
  }
}

impl PartialEq for Vertex {
  fn eq(&self, other: &Self) -> bool {
    self.0 == other.0
  }
}

impl Eq for Vertex {}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m, mut s) = (vs[0], vs[1], vs[2]);
  let mut rs: Vec<HashMap<usize, (usize, usize)>> = vec![HashMap::new(); n];
  let mut max_s = 0;
  for _ in 0..m {
    let line = lines.next().unwrap().unwrap();
    let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
    let u = vs[0] - 1;
    let v = vs[1] - 1;
    rs[u].insert(v, (vs[2], vs[3]));
    rs[v].insert(u, (vs[2], vs[3]));
    max_s = max(max_s, vs[2]);
  }
  max_s = (n - 1) * max_s;
  s = min(max_s, s);
  let mut xs: Vec<(usize, usize)> = Vec::new();
  for _ in 0..n {
    let line = lines.next().unwrap().unwrap();
    let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
    xs.push((vs[0], vs[1]));
  }

  let mut queue = BinaryHeap::new();
  queue.push(Vertex(0, 0, s));
  let mut fixed = HashSet::new();
  let mut ts = vec![vec![usize::max_value(); max_s+1]; n];
  ts[0][s] = 0;
  while let Some(Vertex(t, u, s)) = queue.pop() {
    if fixed.contains(&(u, s)) {
      continue;
    }
    fixed.insert((u, s));
    if s < max_s {
      let s1 = min(max_s, s+xs[u].0);
      let t1 = t + xs[u].1;
      queue.push(Vertex(t1, u, s1));
      ts[u][s1] = min(ts[u][s1], t1);
    }
    for (v, (a, b)) in &rs[u] {
      if *a <= s {
        let s1 = s - *a;
        let t0 = ts[*v][s1];
        let t1 = ts[u][s] + b;
        if t0 == usize::max_value() || t1 < t0 {
          ts[*v][s1] = t1;
        }
        if !fixed.contains(&(*v, s1)) {
          queue.push(Vertex(t1, *v, s1));
        }
      }
    }
  }
  for i in 1..n {
    println!("{}", ts[i].iter().cloned().min().unwrap());
  }
}