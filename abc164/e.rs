use std::cmp::Ordering;
use std::io::{self, BufRead};
use std::collections::{BinaryHeap, HashMap, HashSet};

struct Vertex(usize, usize);

impl Ord for Vertex {
  fn cmp(&self, other: &Self) -> Ordering {
    other.1.cmp(&self.1)
  }
}

impl PartialOrd for Vertex {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(other.1.cmp(&self.1))
  }
}

impl PartialEq for Vertex {
  fn eq(&self, other: &Self) -> bool {
    self.1 == other.1
  }
}

impl Eq for Vertex {}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m, s) = (vs[0], vs[1], vs[2]);
  let mut rs: Vec<HashMap<usize, (usize, usize)>> = vec![HashMap::new(); n];
  for _ in 0..m {
    let line = lines.next().unwrap().unwrap();
    let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
    let u = vs[0] - 1;
    let v = vs[1] - 1;
    rs[u].insert(v, (vs[2], vs[3]));
    rs[v].insert(u, (vs[2], vs[3]));
  }
  let mut xs: Vec<(usize, usize)> = Vec::new();
  for _ in 0..n {
    let line = lines.next().unwrap().unwrap();
    let vs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
    xs.push((vs[0], vs[1]));
  }

  let mut scores: Vec<(usize, usize, Vec<(usize, usize)>)> = vec![(usize::max_value(), 0, Vec::new()); n];
  scores[0] = (0, s, vec![xs[0]]);
  let mut queue = BinaryHeap::new();
  queue.push(Vertex(0, 0));
  let mut fixed = HashSet::new();
  while let Some(Vertex(i, si)) = queue.pop() {
    println!("a {} {}", i, si);
    fixed.insert(i);
    for (j, (a, b)) in &rs[i] {
      println!("  b {} {} {}", j, a, b);
      let sj = si + b;
      if scores[*j].0 == usize::max_value() || sj < scores[*j].0 {
        let mut ys = scores[i].2.clone();
        ys.push(xs[*j]);
        scores[*j] = (sj, s, ys);
      }
      if !fixed.contains(j) {
        queue.push(Vertex(*j, sj));
      }
    }
  }
  println!("{:?}", scores);
}