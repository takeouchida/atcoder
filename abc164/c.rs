use std::io::{self, BufRead};
use std::collections::HashSet;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let x: usize = line.parse().unwrap();
  let mut s: HashSet<String> = HashSet::new();
  for _ in 0..x {
    s.insert(lines.next().unwrap().unwrap());
  }
  println!("{}", s.len());
}