use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: usize = line.parse().unwrap();
  let line = lines.next().unwrap().unwrap();
  let ss: Vec<bool> = line.split(' ').map(|x| x.parse().unwrap()).map(|s: i32| s != 0).collect();
  let line = lines.next().unwrap().unwrap();
  let ts: Vec<bool> = line.split(' ').map(|x| x.parse().unwrap()).map(|s: i32| s != 0).collect();
  let line = lines.next().unwrap().unwrap();
  let us: Vec<u64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<u64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let mut zeros: Vec<Vec<u64>> = vec![vec![0; n]; n];
  let mut ones: Vec<Vec<u64>> = vec![vec![0; n]; n];
  for i in 0..n {
    for j in 0..n {
      if ss[i] {
        zeros[i][j] = !us[i];
      } else {
        ones[i][j] = us[i];
      }
    }
  }
  for i in 0..n {
    for j in 0..n {
      if ts[i] {
        zeros[j][i] |= !vs[i];
      } else {
        ones[j][i] |= vs[i]
      }
    }
  }
  for i in 0..n {
    for j in 0..n {
      if zeros[i][j] & ones[i][j] != 0 {
        println!("-1");
        return;
      }
    }
  }
  let mut row_ones: Vec<u64> = vec![0; n];
  let mut row_zeros: Vec<u64> = vec![0; n];
  let mut col_ones: Vec<u64> = vec![0; n];
  let mut col_zeros: Vec<u64> = vec![0; n];
  for i in 0..n {
    row_ones[i] = (0..n).map(|j| ones[i][j]).fold(0, |a, b| a | b);
    row_zeros[i] = (0..n).map(|j| zeros[i][j]).fold(0, |a, b| a | b);
    col_ones[i] = (0..n).map(|j| ones[j][i]).fold(0, |a, b| a | b);
    col_zeros[i] = (0..n).map(|j| zeros[j][i]).fold(0, |a, b| a | b);
  }
  for i in 0..n {
    if ss[i] {
      for j in 0..n {
        if !ts[j] {
          let x = ones[i][j] | (us[i] & !row_ones[i] & !vs[i] & !col_zeros[j]);
          if x != ones[i][j] {
            ones[i][j] = x;
            col_ones[j] |= x;
            row_ones[i] |= x;
          }
        }
      }
    }
  }
  for j in 0..n {
    if ts[j] {
      for i in 0..n {
        if !ss[i] {
          let x = ones[i][j] | (vs[j] & !col_ones[i] & !us[i] & !row_zeros[i]);
          if x != ones[i][j] {
            ones[i][j] = x;
            col_ones[j] |= x;
            row_ones[i] |= x;
          }
        }
      }
    }
  }
  println!("{:?}", ones);
}