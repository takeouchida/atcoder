use std::io::{self, BufRead};
use std::mem::swap;

const M: usize = 2019;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let nums: Vec<usize> = lines.next().unwrap().unwrap().chars().map(|c| c as usize - 48).collect();
  let mut zeroes = 0;
  let mut counts = vec![0; M];
  for i in 0..nums.len() {
    let mut temp = vec![0; M];
    for j in 0..M {
      let k = ((j * 10 % M) + nums[i]) % M;
      temp[k] = counts[j];
    }
    temp[nums[i]] += 1;
    swap(&mut counts, &mut temp);
    zeroes += counts[0];
  }
  println!("{}", zeroes);
}