use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let cs: Vec<_> = lines.next().unwrap().unwrap().chars().collect();
  let n = cs.len();
  let mut c = cs[0];
  let mut i = 0;
  let mut j = 0;
  let mut buf: Vec<usize> = vec![0; n+1];
  while j < n {
    if c == '<' && (j == n - 1 || cs[j+1] == '>') {
      for k in 0..(j-i+1) {
        buf[i+k+1] = max(k+1, buf[i+k+1]);
      }
      if j < n - 1 {
        c = cs[j+1];
        i = j + 1;
      }
    } else if c == '>' && (j == n - 1 || cs[j+1] == '<') {
      for k in 0..(j-i+1) {
        buf[j-k] = max(k+1, buf[j-k]);
      }
      if j < n - 1 {
        c = cs[j+1];
        i = j + 1;
      }
    }
    j += 1;
  }
  println!("{}", buf.iter().sum::<usize>());
}