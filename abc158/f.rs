use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut input = Vec::<(i32, i32)>::new();
  for (i, line) in stdin.lock().lines().enumerate() {
    if i == 0 {
      continue;
    }
    let s: Vec<i32> = line.unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    input.push((s[0], s[1]));
  }

  input.sort_by(|(x1, _), (x2, _)| x2.cmp(&x1) );

  println!("{:?}", input);

  let mut total_on = 1;
  let mut total_off = 1;
  let mut iter = input.iter();
  let mut x0 = iter.next().unwrap().0;

  for (x, d) in iter {
    let count_on = if x + d > x0 {
      total_on
    } else {
      total_on + total_off
    };
    let count_off = total_on + total_off;
    total_on = count_on;
    total_off = count_off;
    println!("{} {}", total_on, total_off);
    x0 = *x;
  }
  println!("{}", total_on + total_off);
}