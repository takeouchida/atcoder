use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let lines: Vec<String> = stdin.lock().lines().map(|x| x.unwrap()).collect();
  let xs: Vec<i32> = lines[0].split(' ').map(|x| x.parse().unwrap()).collect();
  let p = xs[1];
  let s = &lines[1];
  let bs = s.as_bytes();
  let n = s.len();

  let mut mods: Vec<Vec<i32>> = vec![vec![-1; n]; n];
  for k in 1..(n+1) {
    for i in 0..(n-k+1) {
      let j = i + k;
      let x: i32 = (bs[j-1] as char) as i32 - 48;
      let m0 = if k == 1 { 0 } else { mods[i][j-2] };
      let m1 = (m0 * 10 + x) % p;
      mods[i][j-1] = m1;
    }
  }
  let count = mods.iter().map(|xs| xs.iter().filter(|&x| x == &0).count()).fold(0, |sum, i| sum + i);
  println!("{}", count);
}