use std::io;

fn main() {
  let mut input = String::new();
  if let Ok(_) = io::stdin().read_line(&mut input) {
    let s = input.trim();
    println!("{}", if (s == "AAA" || s == "BBB") { "No" } else { "Yes" });
  }
}