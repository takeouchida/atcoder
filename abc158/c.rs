use std::io;

fn main() {
  let mut input = String::new();
  if let Ok(_) = io::stdin().read_line(&mut input) {
    let v: Vec<i32> = input.trim().split(' ').map(|x| x.parse().unwrap()).collect();
    let o = (1..1001).find(|x| *x * 8 / 100 == v[0] && *x / 10 == v[1]).unwrap_or(-1);
    println!("{}", o);
  }
}