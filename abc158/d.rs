use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut i = 0;
  let mut s = String::new();
  let mut size: i32 = -1;
  for line in stdin.lock().lines() {
    match i {
      0 => s = line.unwrap(),
      1 => size = line.unwrap().parse().unwrap(),
      _ => {
        let xs = line.unwrap();
        let ys: Vec<&str> = xs.split(' ').collect();
        match ys[0] {
          "1" => s = s.chars().rev().collect::<String>(),
          "2" => match ys[1] {
            "1" => {
              let mut tmp = String::from(ys[2]);
              tmp.push_str(&s);
              s = tmp;
            },
            "2" => s.push_str(ys[2]),
            _ => (),
          },
          _ => (),
        }
      },
    }
    i += 1;
    if size >= 0 && size == i - 2 {
      break;
    }
  }
  println!("{}", s);
}