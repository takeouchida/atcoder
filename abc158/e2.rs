use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let lines: Vec<String> = stdin.lock().lines().map(|x| x.unwrap()).collect();
  let xs: Vec<i32> = lines[0].split(' ').map(|x| x.parse().unwrap()).collect();
  let p = xs[1];
  let s = &lines[1];
  let bs = s.as_bytes();
  let n = s.len();
  let mut count = 0;

  for i in 0..n {
    let mut m0 = 0;
    for j in i..n {
      let x = (bs[j] as i32) - 48;
      let m1 = (m0 * 10 + x) % p;
      if m1 == 0 {
        count += 1;
      }
      m0 = m1;
    }
  }

  println!("{}", count);
}