use std::io;
use std::cmp::min;

fn main() {
  let mut input = String::new();
  if let Ok(_) = io::stdin().read_line(&mut input) {
    let v: Vec<i64> = input.trim().split(' ').map(|x| x.parse().unwrap()).collect();
    let count = v[0];
    let unit = v[1] + v[2];
    let blue = v[1];
    let div = count / unit;
    let res = count % unit;
    println!("{}", div * blue + min(res, blue));
  }
}