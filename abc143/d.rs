use std::io::{self, BufRead};

pub fn lower_bound<T: Ord>(vec: &[T], val: T) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

pub fn upper_bound<T: Ord>(vec: &[T], val: T) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] > val {
      hi = p;
    } else {
      lo = p + 1;
    }
  }
  return lo;
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut ls: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  ls.sort();
  let mut count = 0;
  for i in 0..(n-2) {
    for j in (i+1)..(n-1) {
      let x = ls[i];
      let y = ls[j];
      let min = y - x + 1;
      let max = x + y - 1;
      let lo = lower_bound(&ls[(j+1)..], min);
      let hi = upper_bound(&ls[(j+1)..], max);
      count += hi - lo;
    }
  }
  println!("{}", count);
}