use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut paths: usize = 1;
  let mut vs: Vec<usize> = Vec::new();
  if n == 0 {
    println!("{}", if xs[0] == 1 { 1 } else { -1 });
    return;
  }
  for i in 0..n {
    if paths <= xs[i] {
      println!("-1");
      return;
    }
    vs.push(paths - xs[i]);
    paths = (paths - xs[i]) * 2;
  }
  if paths < xs[n] {
    println!("-1");
    return;
  }
  vs[n-1] = min(vs[n-1], xs[n]);
  for i in 1..n {
    vs[n-i-1] = min(vs[n-i-1], xs[n-i] + vs[n-1]);
  }
  let ans: usize = xs.iter().sum::<usize>() + vs.iter().sum::<usize>();
  println!("{}", ans);
}