use std::io::{self, BufRead};
use std::collections::BTreeSet;
use Dir::*;

#[derive(Copy, Clone)]
enum Dir {
  Up, Down, Left, Right
}

impl Dir {
  fn to_char(self) -> u8 {
    match self {
      Up => 'U' as u8,
      Down => 'D' as u8,
      Left => 'L' as u8,
      Right => 'R' as u8,
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut ps: Vec<(i32, i32)> = Vec::new();
  for _ in 0..n {
    let xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    ps.push((xs[0], xs[1]));
  }
  let even = ps.iter().filter(|p| (p.0.abs() + p.1.abs()) % 2 == 0).count();
  if even != 0 && even != ps.len() {
    println!("-1");
    return;
  }
  let len = ps.iter().map(|p| p.0.abs() + p.1.abs()).max().unwrap();
  let mut joints: BTreeSet<i32> = BTreeSet::new();
  for &(px, py) in &ps {
    if px != 0 && py != 0 {
      joints.insert(px.abs() as i32);
    }
    let dist = px.abs() + py.abs();
    if dist != len {
      joints.insert((dist + len) / 2);
    }
  }
  let joints: Vec<i32> = joints.into_iter().collect();
  let mut temp = 0;
  for i in 0..joints.len() {
    print!("{} ", joints[i] - temp);
    temp = joints[i];
  }
  println!("{}", if joints.is_empty() { len } else { len - joints[joints.len()-1] });
  for (px, py) in ps {
    let mut d = if px == 0 {
      if py >= 0 { Up } else { Down }
    } else if px >= 0 {
      Right
    } else {
      Left
    };
    let mut ds: Vec<u8> = vec![d.to_char()];
    let p1 = px.abs();
    let p2 = py.abs();
    let p3 = (len + p1 + p2) / 2;
    for i in 0..joints.len() {
      if p1 == joints[i] {
        d = if py >= 0 { Up } else { Down };
      } else if p3 == joints[i] {
        d = match d {
          Up => Down,
          Down => Up,
          Left => Right,
          Right => Left,
        };
      }
      ds.push(d.to_char());
    }
    println!("{}", String::from_utf8(ds).unwrap());
  }
}