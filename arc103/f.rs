use std::io::{self, BufRead};
use std::collections::BTreeMap;

#[derive(Debug, Clone)]
struct Tree {
  id: usize,
  count: usize,
  children: Vec<Box<Tree>>,
}

fn dfs(t: &Box<Tree>, es: &mut Vec<(usize, usize)>) {
  for c in &t.children {
    es.push((t.id, c.id));
    dfs(&c, es);
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut ds: Vec<(usize, usize)> = Vec::new();
  for i in 0..n {
    let x: usize = lines.next().unwrap().unwrap().parse().unwrap();
    ds.push((i + 1, x));
  }
  ds.sort_by(|x, y| y.1.cmp(&x.1));
  let mut forest: BTreeMap<usize, Vec<Box<Tree>>> = BTreeMap::new();
  for tup in &ds {
    let i = tup.0;
    let d = tup.1;
    let contains = forest.contains_key(&d);
    let v = if contains {
      let cn = forest.remove(&d).unwrap();
      Tree { id: i, count: 1 + cn.iter().map(|c| c.count).sum::<usize>(), children: cn }
    } else {
      Tree { id: i, count: 1, children: Vec::new() }
    };
    let k = d + 2 * v.count - n;
    let contains = forest.contains_key(&k);
    if contains {
      if let Some(t) = forest.get_mut(&k) {
        t.push(Box::new(v));
      }
    } else {
      forest.insert(k, vec![Box::new(v)]);
    }
  }
  if forest.len() != 1 || forest.iter().next().unwrap().1.len() != 1 {
    println!("-1");
    return;
  }
  let mut es: Vec<(usize, usize)> = Vec::new();
  let t = &forest.iter().next().unwrap().1[0];
  dfs(t, &mut es);
  for (x, y) in es {
    println!("{} {}", x, y);
  }
}