use std::io::{self, BufRead};
use std::collections::HashMap;
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let _: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let vs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut xs: HashMap<usize, usize> = HashMap::new();
  let mut ys: HashMap<usize, usize> = HashMap::new();
  for i in 0..vs.len() {
    let k = vs[i];
    if i % 2 == 0 {
      let v = xs.get(&k).unwrap_or(&0).clone();
      xs.insert(k, v + 1);
    } else {
      let v = ys.get(&k).unwrap_or(&0).clone();
      ys.insert(k, v + 1);
    }
  }
  let (kx, vx) = xs.iter().max_by(|x, y| x.1.cmp(y.1)).unwrap();
  let (ky, vy) = ys.iter().max_by(|x, y| x.1.cmp(y.1)).unwrap();
  let nx = (vs.len() + 1) / 2 - vx;
  let ny = vs.len() / 2 - vy;
  let ans = if kx != ky {
    nx + ny
  } else {
    let zero = 0;
    let (_, vx2) = xs.iter().filter(|k| *k.0 != *kx).max_by(|x, y| x.1.cmp(y.1)).unwrap_or((&zero, &zero));
    let (_, vy2) = ys.iter().filter(|k| *k.0 != *ky).max_by(|x, y| x.1.cmp(y.1)).unwrap_or((&zero, &zero));
    let nx2 = (vs.len() + 1) / 2 - vx2;
    let ny2 = vs.len() / 2 - vy2;
    min(nx + ny2, nx2 + ny)
  };
  println!("{}", ans);
}