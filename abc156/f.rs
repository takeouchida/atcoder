use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let k = vs[0] as usize;
  let q = vs[1];
  let line = lines.next().unwrap().unwrap();
  let ds: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  for _ in 0..q {
    let line = lines.next().unwrap().unwrap();
    let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
    let n = vs[0] as usize;
    let m = vs[2];
    let mut a = vs[1] % m;
    let mut count = 0;
    let a0 = a;
    let mut j = 1;
    while j < n {
      let i = (j - 1) % k;
      if j > 1 && i == 0 && a == a0 {
        let period = j - 1;
        let freq = (n - 1) / period;
        count *= freq;
        j += period * (freq - 1);
      }
      let b = (a + ds[i]) % m;
      if a < b {
        count += 1;
      }
      a = b;
      j += 1;
    }
    println!("{}", count);
  }
}