use std::io::{self, BufRead};

const M: i64 = 1000000007;

fn pow(p: i64, n: i64) -> i64 {
  let mut q = n;
  let mut m = p;
  let mut ans = 1;
  while q > 0 {
    if q % 2 == 1 {
      ans = (ans * m) % M;
    }
    q /= 2;
    m = (m * m) % M;
  }
  ans
}

fn div(n: i64, m: i64) -> i64 {
  (n * pow(m, M - 2)) % M
}

fn mul(n: i64, m: i64) -> i64 {
  (n * m) % M
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let k = vs[1];
  let mut facts = vec![1 as i64; (2 * n) as usize];

  for i in 2..(2*n) {
    facts[i as usize] = mul(facts[(i-1) as usize], i);
  }

  let ans = if n < k {
    div(facts[(2 * n - 1) as usize], mul(facts[(n - 1) as usize], facts[n as usize]))
  } else {
    (0..(k+1)).map(|m| div(mul(facts[n as usize], facts[(n - 1) as usize]), mul(mul(mul(facts[m as usize], facts[(n - m) as usize]), facts[if n > m { (n - m - 1) as usize } else { 1 }]), facts[m as usize]))).fold(0, |acc, x| (acc + x) % M)
  };

  println!("{}", ans);
}