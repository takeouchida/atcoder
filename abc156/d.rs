use std::io::{self, BufRead};

const M: i64 = 1000000007;

fn pow(p: i64, n: i64) -> i64 {
  let mut q = n;
  let mut m = p;
  let mut ans = 1;
  while q > 0 {
    if q % 2 == 1 {
      ans = (ans * m) % M;
    }
    q /= 2;
    m = (m * m) % M;
  }
  ans
}

fn sub(a: i64, b: i64) -> i64 {
  (a + M - b) % M
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let a = vs[1];
  let b = vs[2];
  let total = sub(pow(2, n), 1);
  println!("{}", total);
}