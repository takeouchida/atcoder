use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let _: i32 = line.parse().unwrap();
  let line = lines.next().unwrap().unwrap();
  let xs: Vec<i32> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let x_min = xs.iter().min().unwrap();
  let x_max = xs.iter().max().unwrap();
  let e_min: i32 = (*x_min..(*x_max+1)).map(|p| xs.iter().map(|x| (x - p).pow(2)).sum()).min().unwrap();
  println!("{}", e_min);
}