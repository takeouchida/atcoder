use std::io::{self, BufRead};

fn find_index(ps: &Vec<usize>, n: usize) -> usize {
  let mut digits: Vec<usize> = (1..(n+1)).collect();
  let mut total: usize = 0;
  for i in 0..n {
    if let Some(j) = digits.iter().position(|&x| x == ps[i]) {
      let x =  j * (1..(n-i)).product::<usize>();
      total += x;
      digits.remove(j);
    }
  }
  total
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let n: usize = line.parse().unwrap();
  let line = lines.next().unwrap().unwrap();
  let ps: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let line = lines.next().unwrap().unwrap();
  let qs: Vec<usize> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let x = find_index(&ps, n);
  let y = find_index(&qs, n);
  let ans = if x < y { y - x } else { x - y };
  println!("{}", ans);
}