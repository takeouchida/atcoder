use std::io::{self, BufRead};
use std::collections::HashMap;

pub fn find_prime_factors(n: i64) -> HashMap<i64, i64> {
  let mut res = HashMap::new();
  let mut i = 2;
  let mut m = n;
  while i * i <= m {
    while m % i == 0 {
      let val = 1 + res.get(&i).unwrap_or(&0);
      res.insert(i, val);
      m /= i;
    }
    i += 1
  }
  if m != 1 {
    let val = 1 + res.get(&m).unwrap_or(&0);
    res.insert(m, val);
  }
  res
}

pub fn merge(x: &mut HashMap<i64, i64>, y: &HashMap<i64, i64>) {
  for (p, n) in y {
    let o = x.get(p).map(|q| *q);
    if let Some(q) = o {
      if q < *n {
        x.insert(*p, *n);
      }
    } else {
      x.insert(*p, *n);
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let line = lines.next().unwrap().unwrap();
  let vs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let n = vs[0];
  let m = vs[1] * 2;
  let line = lines.next().unwrap().unwrap();
  let xs: Vec<i64> = line.split(' ').map(|x| x.parse().unwrap()).collect();
  let mut lcm = find_prime_factors(xs[0]);
  for i in 1..n {
    merge(&mut lcm, &find_prime_factors(xs[i as usize]));
  }
  let lcm: i64 = lcm.iter().map(|(p, n)| p * n).product();
  let count = (m / lcm + 1) / 2;
  println!("{}", count);
}