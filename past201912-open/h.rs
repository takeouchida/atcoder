use std::io::{self, BufRead};
use std::cmp::min;

#[derive(Debug)]
enum Query {
  Single(usize, usize),
  Set(usize),
  All(usize),
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut cs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let q: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut qs: Vec<Query> = Vec::new();
  for _ in 0..q {
    let ss: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    match ss[0] {
      1 => qs.push(Query::Single(ss[1] - 1, ss[2])),
      2 => qs.push(Query::Set(ss[1])),
      3 => qs.push(Query::All(ss[1])),
      _ => panic!("Command not match")
    }
  }
  let mut sold_single: usize = 0;
  let mut sold_set: usize = 0;
  let mut sold_all: usize = 0;
  let mut min_odd: usize = usize::max_value();
  let mut min_all: usize = usize::max_value();
  for i in 0..n {
    min_all = min(min_all, cs[i]);
    if i % 2 == 0 {
      min_odd = min(min_odd, cs[i]);
    }
  }
  for q in qs {
    match q {
      Query::Single(x, a) => {
        let s = if x % 2 == 0 { sold_set } else { 0 };
        let c = cs[x] - sold_all - s;
        if a <= c {
          sold_single += a;
          cs[x] -= a;
          min_all = min(min_all, c - a);
          if x % 2 == 0 {
            min_odd = min(min_odd, c - a);
          }
        }
      },
      Query::Set(a) => {
        if a <= min_odd {
          min_odd -= a;
          sold_set += a;
          min_all = min(min_all, min_odd);
        }
      },
      Query::All(a) => {
        if a <= min_all {
          min_all -= a;
          sold_all += a;
        }
      },
    }
  };
  println!("{}", sold_single + sold_set * ((n + 1) / 2) + sold_all * n);
}