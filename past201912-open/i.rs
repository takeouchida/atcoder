use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1]);
  let cases: usize = (2 as usize).pow(n as u32);
  let mut memo: Vec<Option<u64>> = vec![None; cases];
  let mut sets: Vec<(usize, u64)> = Vec::new();
  for _ in 0..m {
    let ss = lines.next().unwrap().unwrap();
    let xs: Vec<_> = ss.split(' ').collect();
    let s: usize = xs[0].chars().enumerate().map(|(i, x)| if x == 'Y' { 1 << i } else { 0 }).sum();
    let c: u64 = xs[1].parse().unwrap();
    sets.push((s, c));
  }
  for &(s, c) in &sets {
    memo[s] = Some(c);
  }
  for &(s, c) in &sets {
    for i in 0..cases {
      if let Some(d) = memo[i] {
        let j = i | s;
        memo[j] = match memo[j] {
          None => Some(c + d),
          Some(e) => Some(min(e, c + d)),
        };
      }
    }
  }
  println!("{}", match memo[cases-1] { None => -1, Some(c) => c as i64 });
}