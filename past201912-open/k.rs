use std::io::{self, BufRead};

fn dfs(depths: &mut Vec<usize>, es: &Vec<Vec<usize>>, n: usize, d: usize) {
  depths[n] = d;
  for &e in &es[n] {
    dfs(depths, es, e, d + 1);
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut ps: Vec<isize> = Vec::new();
  let mut es: Vec<Vec<usize>> = vec![vec![]; n];
  let mut root: usize = 0;
  for i in 0..n {
    let p: isize = lines.next().unwrap().unwrap().parse().unwrap();
    ps.push(p);
    if p >= 0 {
      es[p as usize - 1].push(i);
    } else {
      root = i;
    }
  }
  let q: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut qs: Vec<(usize, usize)> = Vec::new();
  for _ in 0..q {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    qs.push((xs[0] - 1, xs[1] - 1));
  }
  let mut log_n = 0;
  let mut temp = 1;
  while temp <= n {
    log_n += 1;
    temp <<= 1;
  }
  let mut memo: Vec<Vec<Option<usize>>> = vec![vec![None; n]; log_n];
  for i in 0..n {
    if ps[i] >= 0 {
      memo[0][i] = Some(ps[i] as usize - 1);
    }
  }
  for i in 1..log_n {
    for j in 0..n {
      if let Some(k) = memo[i-1][j] {
        memo[i][j] = memo[i-1][k];
      }
    }
  }
  let mut depths: Vec<usize> = vec![0; n];
  dfs(&mut depths, &es, root, 0);
  for &(a, b) in &qs {
    if depths[a] < depths[b] {
      println!("No");
      continue;
    }
    let dd = depths[a] - depths[b];
    let mut p = Some(a);
    for i in 0..log_n {
      if dd & (1 << i) != 0 {
        if let Some(c) = p {
          p = memo[i][c];
        }
      }
    }
    println!("{}", if p == Some(b) { "Yes" } else { "No" });
  }
}