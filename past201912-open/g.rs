use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut xss: Vec<Vec<i32>> = vec![vec![0; n]; n - 1];
  for i in 0..(n-1) {
    let ys: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    for j in 0..(n-i-1) {
      let k = j + i + 1;
      xss[i][k] = ys[j];
    }
  }
  let mut cases = 1;
  for _ in 0..n {
    cases *= 3;
  }
  let mut ans: i32 = i32::min_value();
  for i in 0..cases {
    let mut ixs: Vec<usize> = vec![0; n];
    let mut k = i;
    for j in 0..n {
      ixs[j] = k % 3;
      k /= 3;
    }
    let mut sum: i32 = 0;
    for j in 0..(n-1) {
      for k in (j+1)..n {
        if ixs[j] == ixs[k] {
          sum += xss[j][k];
        }
      }
    }
    ans = max(ans, sum);
  }
  println!("{}", if ans == i32::min_value() { 0 } else { ans });
}